import MySQLDriver
from Helper import write_results
from Helper import write_results_wid
from Helper import rankings_uneven
from random import randint
from grading import RankingGrading

STRONGER_OPP = 0
EQUAL = 1
WEAKER_OPP = 2
BOUND = 6
MIN_AWARD = 0.1
MAX_AWARD = 5
MIN_LOSS = -0.1
MAX_LOSS = -5
INITIAL_RATING = 70

ele_teams = []


def team_rating_diff(home_team, away_team, home_adv):
    """Finds the rating difference between two given teams.

        Args:
            home_team: The Home team's name.
            away_team: The Away team's name.
            home_adv: Home advantage factor.
        Returns:
            The Rating Difference of the given teams.
    """
    ele_teams.sort(key=lambda x: x[1])  # sort on points
    home_team_rating = -1
    away_team_rating = -1
    for i in range(len(ele_teams)):
        if ele_teams[i][0] == home_team:
            home_team_rating = ele_teams[i][1]
        if ele_teams[i][0] == away_team:
            away_team_rating = ele_teams[i][1]
    return home_team_rating + home_adv - away_team_rating


def ext_elephant_ranking(league, year, teams, list_of_matches, win_pts, draw_pts, loss_pts, match_factor, home_adv,
                         scaled, num_of_matches, rank_id, league_table, exp_id):
    """Elephant Ratings System (to be used externally - see next function to be used internally).

        Teams start with points according to first World Cup appearance).
        70 (1950), 60 (1954), 50 (1958), 40 (1962), 30 (1966+)
        Formulae:
            NewRating = OldRating + new_pts
        see: http://rsport.netorn.ru/ech/theory/elephant.htm

        Args:
            league: The league under investigation. (see: leagues.txt)
            year: The year (season) used for the rankings.
            teams: Dictionary of the participating teams in the league for given year.
            list_of_matches: Supplied list of matches data used for the rankings.
            win_pts: The win points vector. (default: [5, 3, 1])
            draw_pts: The draw points vector. (default: [2, 0, -2])
            loss_pts: The loss points vector. (default: [-1, -3, -5])
            match_factor: Match importance factor. # default: under investigation
            home_adv: Home advantage adjustment. # (custom) default: 0
            scaled: (Boolean) if true, it awards points in a scaled manner acc. to the team rating difference.
            num_of_matches: Total number of matches in our matches dataset.
            rank_id: The ranking id (used to identify it in DB for grading).
            league_table: The final league table (should be auto-generated).
            exp_id: The experiment id.

        Returns:
            A list demonstrating the Elephant Rankings.
    """
    ele_teams[:] = []

    for team in teams:
        ele_teams.append([team, INITIAL_RATING])
    ele_teams.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(ele_teams)):
            if ele_teams[i][0] == match[2]:
                home_team = i
            elif ele_teams[i][0] == match[3]:
                away_team = i

        rating_diff = team_rating_diff(ele_teams[home_team][0], ele_teams[away_team][0], home_adv)
        if match[6][0][:1] == "H":
            if rating_diff > 0:
                if scaled:
                    if win_pts[EQUAL] - MIN_AWARD * rating_diff < MIN_AWARD:
                        ele_teams[home_team][1] += MIN_AWARD
                    else:
                        ele_teams[home_team][1] += win_pts[EQUAL] - MIN_AWARD * rating_diff

                    if loss_pts[EQUAL] - MIN_LOSS * rating_diff > MIN_LOSS:
                        ele_teams[away_team][1] += MIN_LOSS
                    else:
                        ele_teams[away_team][1] += (loss_pts[EQUAL] - MIN_LOSS * rating_diff)
                else:
                    ele_teams[home_team][1] += win_pts[WEAKER_OPP]
                    ele_teams[away_team][1] += loss_pts[STRONGER_OPP]
            elif rating_diff == 0:
                    ele_teams[home_team][1] += win_pts[EQUAL]
                    ele_teams[away_team][1] += loss_pts[EQUAL]
            else:
                if scaled:
                    if win_pts[EQUAL] + MIN_AWARD * abs(rating_diff) > MAX_AWARD:
                        ele_teams[home_team][1] += MAX_AWARD
                    else:
                        ele_teams[home_team][1] += win_pts[EQUAL] + MIN_AWARD * abs(rating_diff)

                    if loss_pts[EQUAL] + MIN_LOSS * abs(rating_diff) < MAX_LOSS:
                        ele_teams[away_team][1] += MAX_LOSS
                    else:
                        ele_teams[away_team][1] += loss_pts[EQUAL] + MIN_LOSS * abs(rating_diff)
                else:
                    ele_teams[home_team][1] += win_pts[STRONGER_OPP]
                    ele_teams[away_team][1] += loss_pts[WEAKER_OPP]
        elif match[6][0][:1] == "A":
            if rating_diff > 0:
                if scaled:
                    if win_pts[EQUAL] + MIN_AWARD * rating_diff > MAX_AWARD:
                        ele_teams[away_team][1] += MAX_AWARD
                    else:
                        ele_teams[away_team][1] += win_pts[EQUAL] + MIN_AWARD * rating_diff

                    if loss_pts[EQUAL] + MIN_LOSS * rating_diff < MAX_LOSS:
                        ele_teams[home_team][1] += MAX_LOSS
                    else:
                        ele_teams[home_team][1] += loss_pts[EQUAL] + MIN_LOSS * rating_diff
                else:
                    ele_teams[home_team][1] += loss_pts[WEAKER_OPP]
                    ele_teams[away_team][1] += win_pts[STRONGER_OPP]
            elif rating_diff == 0:
                ele_teams[home_team][1] += loss_pts[EQUAL]
                ele_teams[away_team][1] += win_pts[EQUAL]
            else:
                if scaled:
                    if win_pts[EQUAL] - MIN_AWARD * abs(rating_diff) < MIN_AWARD:
                        ele_teams[away_team][1] += MIN_AWARD
                    else:
                        ele_teams[away_team][1] += win_pts[EQUAL] - MIN_AWARD * abs(rating_diff)

                    if loss_pts[EQUAL] - MIN_LOSS * abs(rating_diff) > MIN_LOSS:
                        ele_teams[home_team][1] += MIN_LOSS
                    else:
                        ele_teams[home_team][1] += loss_pts[EQUAL] - MIN_LOSS * abs(rating_diff)
                else:
                    ele_teams[home_team][1] += loss_pts[STRONGER_OPP]
                    ele_teams[away_team][1] += win_pts[WEAKER_OPP]
        else:
            if rating_diff > 0:
                if scaled:
                    if draw_pts[EQUAL] - MIN_AWARD * rating_diff < draw_pts[WEAKER_OPP]:
                        ele_teams[home_team][1] += MAX_LOSS / 2
                    else:
                        ele_teams[home_team][1] += draw_pts[EQUAL] - MIN_AWARD * rating_diff

                    if draw_pts[EQUAL] + MIN_AWARD * rating_diff > draw_pts[STRONGER_OPP]:
                        ele_teams[away_team][1] += MAX_AWARD / 2
                    else:
                        ele_teams[away_team][1] += draw_pts[EQUAL] + MIN_AWARD * rating_diff
                else:
                    ele_teams[home_team][1] += draw_pts[WEAKER_OPP]
                    ele_teams[away_team][1] += draw_pts[STRONGER_OPP]
            elif rating_diff == 0:
                ele_teams[home_team][1] += draw_pts[EQUAL]
                ele_teams[away_team][1] += draw_pts[EQUAL]
            else:
                if scaled:
                    if draw_pts[EQUAL] + MIN_AWARD * abs(rating_diff) > draw_pts[STRONGER_OPP]:
                        ele_teams[home_team][1] += MAX_AWARD / 2
                    else:
                        ele_teams[home_team][1] += draw_pts[EQUAL] + MIN_AWARD * abs(rating_diff)

                    if draw_pts[EQUAL] - MIN_AWARD * abs(rating_diff) < draw_pts[WEAKER_OPP]:
                        ele_teams[home_team][1] += MAX_LOSS / 2
                    else:
                        ele_teams[home_team][1] += draw_pts[EQUAL] - MIN_AWARD * abs(rating_diff)
                else:
                    ele_teams[home_team][1] += draw_pts[STRONGER_OPP]
                    ele_teams[away_team][1] += draw_pts[WEAKER_OPP]

    ele_teams.sort(key=lambda x: x[1])
    var = "ID: %d Win Pts: %s, Draw Pts: %s, Loss Pts: %s\nVars: NumOfMatches: %d-Match Factor: %d-HomeAdv: %d (ext)" \
          "Scaled: %s" % (rank_id, win_pts, draw_pts, loss_pts, num_of_matches, match_factor, home_adv, scaled)

    grading = RankingGrading.grade_ranking(league_table, ele_teams[::-1])

    write_results_wid(ele_teams[::-1], "elephant_ranking" + ("S" if scaled else ""), league, year, var, num_of_matches,
                      grading, rank_id, exp_id)


def recursive_elephant_ranking(league, year, teams, list_of_matches, win_pts, draw_pts, loss_pts, match_factor,
                               home_adv, scaled, num_of_matches, rank_id, league_table, exp_id):
    """Elephant Ratings System  - Recursive Ranking until no further change

        Args:
            Same as ext_ version

        Returns:
            A list demonstrating the Elephant Rankings.
    """
    ele_teams[:] = []

    for team in teams:
        ele_teams.append([team, INITIAL_RATING])
    ele_teams.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(ele_teams)):
            if ele_teams[i][0] == match[2]:
                home_team = i
            elif ele_teams[i][0] == match[3]:
                away_team = i

        rating_diff = team_rating_diff(ele_teams[home_team][0], ele_teams[away_team][0], home_adv)
        if match[6][0][:1] == "H":
            if rating_diff > 0:
                if scaled:
                    if win_pts[EQUAL] - MIN_AWARD * rating_diff < MIN_AWARD:
                        ele_teams[home_team][1] += MIN_AWARD
                    else:
                        ele_teams[home_team][1] += win_pts[EQUAL] - MIN_AWARD * rating_diff

                    if loss_pts[EQUAL] - MIN_LOSS * rating_diff > MIN_LOSS:
                        ele_teams[away_team][1] += MIN_LOSS
                    else:
                        ele_teams[away_team][1] += (loss_pts[EQUAL] - MIN_LOSS * rating_diff)
                else:
                    ele_teams[home_team][1] += win_pts[WEAKER_OPP]
                    ele_teams[away_team][1] += loss_pts[STRONGER_OPP]
            elif rating_diff == 0:
                    ele_teams[home_team][1] += win_pts[EQUAL]
                    ele_teams[away_team][1] += loss_pts[EQUAL]
            else:
                if scaled:
                    if win_pts[EQUAL] + MIN_AWARD * abs(rating_diff) > MAX_AWARD:
                        ele_teams[home_team][1] += MAX_AWARD
                    else:
                        ele_teams[home_team][1] += win_pts[EQUAL] + MIN_AWARD * abs(rating_diff)

                    if loss_pts[EQUAL] + MIN_LOSS * abs(rating_diff) < MAX_LOSS:
                        ele_teams[away_team][1] += MAX_LOSS
                    else:
                        ele_teams[away_team][1] += loss_pts[EQUAL] + MIN_LOSS * abs(rating_diff)
                else:
                    ele_teams[home_team][1] += win_pts[STRONGER_OPP]
                    ele_teams[away_team][1] += loss_pts[WEAKER_OPP]
        elif match[6][0][:1] == "A":
            if rating_diff > 0:
                if scaled:
                    if win_pts[EQUAL] + MIN_AWARD * rating_diff > MAX_AWARD:
                        ele_teams[away_team][1] += MAX_AWARD
                    else:
                        ele_teams[away_team][1] += win_pts[EQUAL] + MIN_AWARD * rating_diff

                    if loss_pts[EQUAL] + MIN_LOSS * rating_diff < MAX_LOSS:
                        ele_teams[home_team][1] += MAX_LOSS
                    else:
                        ele_teams[home_team][1] += loss_pts[EQUAL] + MIN_LOSS * rating_diff
                else:
                    ele_teams[home_team][1] += loss_pts[WEAKER_OPP]
                    ele_teams[away_team][1] += win_pts[STRONGER_OPP]
            elif rating_diff == 0:
                ele_teams[home_team][1] += loss_pts[EQUAL]
                ele_teams[away_team][1] += win_pts[EQUAL]
            else:
                if scaled:
                    if win_pts[EQUAL] - MIN_AWARD * abs(rating_diff) < MIN_AWARD:
                        ele_teams[away_team][1] += MIN_AWARD
                    else:
                        ele_teams[away_team][1] += win_pts[EQUAL] - MIN_AWARD * abs(rating_diff)

                    if loss_pts[EQUAL] - MIN_LOSS * abs(rating_diff) > MIN_LOSS:
                        ele_teams[home_team][1] += MIN_LOSS
                    else:
                        ele_teams[home_team][1] += loss_pts[EQUAL] - MIN_LOSS * abs(rating_diff)
                else:
                    ele_teams[home_team][1] += loss_pts[STRONGER_OPP]
                    ele_teams[away_team][1] += win_pts[WEAKER_OPP]
        else:
            if rating_diff > 0:
                if scaled:
                    if draw_pts[EQUAL] - MIN_AWARD * rating_diff < MAX_LOSS / 2:
                        ele_teams[home_team][1] += MAX_LOSS / 2
                    else:
                        ele_teams[home_team][1] += draw_pts[EQUAL] - MIN_AWARD * rating_diff

                    if draw_pts[EQUAL] + MIN_AWARD * rating_diff > MAX_AWARD / 2:
                        ele_teams[away_team][1] += MAX_AWARD / 2
                    else:
                        ele_teams[away_team][1] += draw_pts[EQUAL] + MIN_AWARD * rating_diff
                else:
                    ele_teams[home_team][1] += draw_pts[WEAKER_OPP]
                    ele_teams[away_team][1] += draw_pts[STRONGER_OPP]
            elif rating_diff == 0:
                ele_teams[home_team][1] += draw_pts[EQUAL]
                ele_teams[away_team][1] += draw_pts[EQUAL]
            else:
                if scaled:
                    if draw_pts[EQUAL] + MIN_AWARD * abs(rating_diff) > MAX_AWARD / 2:
                        ele_teams[home_team][1] += MAX_AWARD / 2
                    else:
                        ele_teams[home_team][1] += draw_pts[EQUAL] + MIN_AWARD * abs(rating_diff)

                    if draw_pts[EQUAL] - MIN_AWARD * abs(rating_diff) < MAX_LOSS / 2:
                        ele_teams[home_team][1] += MAX_LOSS / 2
                    else:
                        ele_teams[home_team][1] += draw_pts[EQUAL] - MIN_AWARD * abs(rating_diff)
                else:
                    ele_teams[home_team][1] += draw_pts[STRONGER_OPP]
                    ele_teams[away_team][1] += draw_pts[WEAKER_OPP]

    ele_teams.sort(key=lambda x: x[1])

    prev_ranking = []
    next_ranking = ele_teams[::-1]

    # while previous ranking is not the same with the re-ranking
    # we re-rank again until no changes occur in the ranking
    while rankings_uneven(prev_ranking, next_ranking):
        prev_ranking = next_ranking
        for match in list_of_matches:
            home_team = -1
            away_team = -1
            for i in range(len(next_ranking)):
                if next_ranking[i][0] == match[2]:
                    home_team = i
                elif next_ranking[i][0] == match[3]:
                    away_team = i

            rating_diff = team_rating_diff(next_ranking[home_team][0], next_ranking[away_team][0], home_adv)
            if match[6][0][:1] == "H":
                if rating_diff > 0:
                    if scaled:
                        if win_pts[EQUAL] - MIN_AWARD * rating_diff < MIN_AWARD:
                            next_ranking[home_team][1] += MIN_AWARD
                        else:
                            next_ranking[home_team][1] += win_pts[EQUAL] - MIN_AWARD * rating_diff

                        if loss_pts[EQUAL] - MIN_LOSS * rating_diff > MIN_LOSS:
                            next_ranking[away_team][1] += MIN_LOSS
                        else:
                            next_ranking[away_team][1] += (loss_pts[EQUAL] - MIN_LOSS * rating_diff)
                    else:
                        next_ranking[home_team][1] += win_pts[WEAKER_OPP]
                        next_ranking[away_team][1] += loss_pts[STRONGER_OPP]
                elif rating_diff == 0:
                        next_ranking[home_team][1] += win_pts[EQUAL]
                        next_ranking[away_team][1] += loss_pts[EQUAL]
                else:
                    if scaled:
                        if win_pts[EQUAL] + MIN_AWARD * abs(rating_diff) > MAX_AWARD:
                            next_ranking[home_team][1] += MAX_AWARD
                        else:
                            next_ranking[home_team][1] += win_pts[EQUAL] + MIN_AWARD * abs(rating_diff)

                        if loss_pts[EQUAL] + MIN_LOSS * abs(rating_diff) < MAX_LOSS:
                            next_ranking[away_team][1] += MAX_LOSS
                        else:
                            next_ranking[away_team][1] += loss_pts[EQUAL] + MIN_LOSS * abs(rating_diff)
                    else:
                        next_ranking[home_team][1] += win_pts[STRONGER_OPP]
                        next_ranking[away_team][1] += loss_pts[WEAKER_OPP]
            elif match[6][0][:1] == "A":
                if rating_diff > 0:
                    if scaled:
                        if win_pts[EQUAL] + MIN_AWARD * rating_diff > MAX_AWARD:
                            next_ranking[away_team][1] += MAX_AWARD
                        else:
                            next_ranking[away_team][1] += win_pts[EQUAL] + MIN_AWARD * rating_diff

                        if loss_pts[EQUAL] + MIN_LOSS * rating_diff < MAX_LOSS:
                            next_ranking[home_team][1] += MAX_LOSS
                        else:
                            next_ranking[home_team][1] += loss_pts[EQUAL] + MIN_LOSS * rating_diff
                    else:
                        next_ranking[home_team][1] += loss_pts[WEAKER_OPP]
                        next_ranking[away_team][1] += win_pts[STRONGER_OPP]
                elif rating_diff == 0:
                    next_ranking[home_team][1] += loss_pts[EQUAL]
                    next_ranking[away_team][1] += win_pts[EQUAL]
                else:
                    if scaled:
                        if win_pts[EQUAL] - MIN_AWARD * abs(rating_diff) < MIN_AWARD:
                            next_ranking[away_team][1] += MIN_AWARD
                        else:
                            next_ranking[away_team][1] += win_pts[EQUAL] - MIN_AWARD * abs(rating_diff)

                        if loss_pts[EQUAL] - MIN_LOSS * abs(rating_diff) > MIN_LOSS:
                            next_ranking[home_team][1] += MIN_LOSS
                        else:
                            next_ranking[home_team][1] += loss_pts[EQUAL] - MIN_LOSS * abs(rating_diff)
                    else:
                        next_ranking[home_team][1] += loss_pts[STRONGER_OPP]
                        next_ranking[away_team][1] += win_pts[WEAKER_OPP]
            else:
                if rating_diff > 0:
                    if scaled:
                        if draw_pts[EQUAL] - MIN_AWARD * rating_diff < MAX_LOSS / 2:
                            next_ranking[home_team][1] += MAX_LOSS / 2
                        else:
                            next_ranking[home_team][1] += draw_pts[EQUAL] - MIN_AWARD * rating_diff

                        if draw_pts[EQUAL] + MIN_AWARD * rating_diff > MAX_AWARD / 2:
                            next_ranking[away_team][1] += MAX_AWARD / 2
                        else:
                            next_ranking[away_team][1] += draw_pts[EQUAL] + MIN_AWARD * rating_diff
                    else:
                        next_ranking[home_team][1] += draw_pts[WEAKER_OPP]
                        next_ranking[away_team][1] += draw_pts[STRONGER_OPP]
                elif rating_diff == 0:
                    next_ranking[home_team][1] += draw_pts[EQUAL]
                    next_ranking[away_team][1] += draw_pts[EQUAL]
                else:
                    if scaled:
                        if draw_pts[EQUAL] + MIN_AWARD * abs(rating_diff) > MAX_AWARD / 2:
                            next_ranking[home_team][1] += MAX_AWARD / 2
                        else:
                            next_ranking[home_team][1] += draw_pts[EQUAL] + MIN_AWARD * abs(rating_diff)

                        if draw_pts[EQUAL] - MIN_AWARD * abs(rating_diff) < MAX_LOSS / 2:
                            next_ranking[home_team][1] += MAX_LOSS / 2
                        else:
                            next_ranking[home_team][1] += draw_pts[EQUAL] - MIN_AWARD * abs(rating_diff)
                    else:
                        next_ranking[home_team][1] += draw_pts[STRONGER_OPP]
                        next_ranking[away_team][1] += draw_pts[WEAKER_OPP]

    next_ranking.sort(key=lambda x: x[1])

    var = "ID: %d Win Pts: %s, Draw Pts: %s, Loss Pts: %s\nVars: NumOfMatches: %d-Match Factor: %d-HomeAdv: %d (rec)" \
          "Scaled: %s" % (rank_id, win_pts, draw_pts, loss_pts, num_of_matches, match_factor, home_adv, scaled)

    grading = RankingGrading.grade_ranking(league_table, next_ranking[::-1])

    write_results_wid(next_ranking[::-1], "elephant_ranking" + ("S" if scaled else ""), league, year, var,
                      num_of_matches, grading, rank_id, exp_id)


def elephant_ranking(league, year, win_pts, draw_pts, loss_pts, match_factor, home_adv, scaled, random, rang):
    """Same as previous functions but only internal use (mainly for testing).
        random: Select random number of matches per team with upper bound rang. (default: False)
        rang: Total number of matches to select for each team or upper bound if random selection.
    """
    match_set_num = rang
    if random:
        match_set_num = randint(1, rang)

    teams = MySQLDriver.get_all_league_teams_for_year(league, year)
    for team in teams:
        ele_teams.append([team, INITIAL_RATING])
    ele_teams.sort()
    
    all_matches = set()

    for team in teams:
        team_matches = MySQLDriver.get_random_matches_for_team(team, league, year, match_set_num)
        for match in team_matches:
            all_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
        del team_matches[:]
    
    list_of_matches = list(all_matches)
    list_of_matches.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(ele_teams)):
            if ele_teams[i][0] == match[2]:
                home_team = i
            elif ele_teams[i][0] == match[3]:
                away_team = i

        rating_diff = team_rating_diff(ele_teams[home_team][0], ele_teams[away_team][0], home_adv)
        if match[6][0][:1] == "H":
            if rating_diff > 0:
                if scaled:
                    if win_pts[EQUAL] - MIN_AWARD * rating_diff < MIN_AWARD:
                        ele_teams[home_team][1] += MIN_AWARD
                    else:
                        ele_teams[home_team][1] += win_pts[EQUAL] - MIN_AWARD * rating_diff

                    if loss_pts[EQUAL] - MIN_LOSS * rating_diff > MIN_LOSS:
                        ele_teams[away_team][1] += MIN_LOSS
                    else:
                        ele_teams[away_team][1] += (loss_pts[EQUAL] - MIN_LOSS * rating_diff)
                else:
                    ele_teams[home_team][1] += win_pts[WEAKER_OPP]
                    ele_teams[away_team][1] += loss_pts[STRONGER_OPP]
            elif rating_diff == 0:
                    ele_teams[home_team][1] += win_pts[EQUAL]
                    ele_teams[away_team][1] += loss_pts[EQUAL]
            else:
                if scaled:
                    if win_pts[EQUAL] + MIN_AWARD * abs(rating_diff) > MAX_AWARD:
                        ele_teams[home_team][1] += MAX_AWARD
                    else:
                        ele_teams[home_team][1] += win_pts[EQUAL] + MIN_AWARD * abs(rating_diff)

                    if loss_pts[EQUAL] + MIN_LOSS * abs(rating_diff) < MAX_LOSS:
                        ele_teams[away_team][1] += MAX_LOSS
                    else:
                        ele_teams[away_team][1] += loss_pts[EQUAL] + MIN_LOSS * abs(rating_diff)
                else:
                    ele_teams[home_team][1] += win_pts[STRONGER_OPP]
                    ele_teams[away_team][1] += loss_pts[WEAKER_OPP]
        elif match[6][0][:1] == "A":
            if rating_diff > 0:
                if scaled:
                    if win_pts[EQUAL] + MIN_AWARD * rating_diff > MAX_AWARD:
                        ele_teams[away_team][1] += MAX_AWARD
                    else:
                        ele_teams[away_team][1] += win_pts[EQUAL] + MIN_AWARD * rating_diff

                    if loss_pts[EQUAL] + MIN_LOSS * rating_diff < MAX_LOSS:
                        ele_teams[home_team][1] += MAX_LOSS
                    else:
                        ele_teams[home_team][1] += loss_pts[EQUAL] + MIN_LOSS * rating_diff
                else:
                    ele_teams[home_team][1] += loss_pts[WEAKER_OPP]
                    ele_teams[away_team][1] += win_pts[STRONGER_OPP]
            elif rating_diff == 0:
                ele_teams[home_team][1] += loss_pts[EQUAL]
                ele_teams[away_team][1] += win_pts[EQUAL]
            else:
                if scaled:
                    if win_pts[EQUAL] - MIN_AWARD * abs(rating_diff) < MIN_AWARD:
                        ele_teams[away_team][1] += MIN_AWARD
                    else:
                        ele_teams[away_team][1] += win_pts[EQUAL] - MIN_AWARD * abs(rating_diff)

                    if loss_pts[EQUAL] - MIN_LOSS * abs(rating_diff) > MIN_LOSS:
                        ele_teams[home_team][1] += MIN_LOSS
                    else:
                        ele_teams[home_team][1] += loss_pts[EQUAL] - MIN_LOSS * abs(rating_diff)
                else:
                    ele_teams[home_team][1] += loss_pts[STRONGER_OPP]
                    ele_teams[away_team][1] += win_pts[WEAKER_OPP]
        else:
            if rating_diff > 0:
                if scaled:
                    if draw_pts[EQUAL] - MIN_AWARD * rating_diff < MAX_LOSS / 2:
                        ele_teams[home_team][1] += MAX_LOSS / 2
                    else:
                        ele_teams[home_team][1] += draw_pts[EQUAL] - MIN_AWARD * rating_diff

                    if draw_pts[EQUAL] + MIN_AWARD * rating_diff > MAX_AWARD / 2:
                        ele_teams[away_team][1] += MAX_AWARD / 2
                    else:
                        ele_teams[away_team][1] += draw_pts[EQUAL] + MIN_AWARD * rating_diff
                else:
                    ele_teams[home_team][1] += draw_pts[WEAKER_OPP]
                    ele_teams[away_team][1] += draw_pts[STRONGER_OPP]
            elif rating_diff == 0:
                ele_teams[home_team][1] += draw_pts[EQUAL]
                ele_teams[away_team][1] += draw_pts[EQUAL]
            else:
                if scaled:
                    if draw_pts[EQUAL] + MIN_AWARD * abs(rating_diff) > MAX_AWARD / 2:
                        ele_teams[home_team][1] += MAX_AWARD / 2
                    else:
                        ele_teams[home_team][1] += draw_pts[EQUAL] + MIN_AWARD * abs(rating_diff)

                    if draw_pts[EQUAL] - MIN_AWARD * abs(rating_diff) < MAX_LOSS / 2:
                        ele_teams[home_team][1] += MAX_LOSS / 2
                    else:
                        ele_teams[home_team][1] += draw_pts[EQUAL] - MIN_AWARD * abs(rating_diff)
                else:
                    ele_teams[home_team][1] += draw_pts[STRONGER_OPP]
                    ele_teams[away_team][1] += draw_pts[WEAKER_OPP]

    ele_teams.sort(key=lambda x: x[1])
    var = "Win Pts: %s, Draw Pts: %s, Loss Pts: %s\nVars: Random: %s - NumOfMatches: %d-Match Factor: %d-HomeAdv: %d" \
          "Scaled: %s" % (win_pts, draw_pts, loss_pts, random, rang, match_factor, home_adv, scaled)
    write_results(ele_teams[::-1], "elephant_ranking", league, year, var)


def main():
    elephant_ranking("E0", 2013, [5, 3, 1], [2, 0, -2], [-1, -3, -5], 0, 4, True, False, 38)


if __name__ == "__main__": main()