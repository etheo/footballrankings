import MySQLDriver
from Helper import write_results
from Helper import write_results_wid
from Helper import rankings_uneven
from random import randint
from grading import RankingGrading

goal_diff_perc = [[0.47, 0.15, 0.08, 0.04, 0.03, 0.02, 0.01],
                  [0.5, 0.16, 0.089, 0.048, 0.037, 0.026, 0.015],
                  [0.51, 0.17, 0.098, 0.056, 0.044, 0.032, 0.02],
                  [0.52, 0.18, 0.107, 0.064, 0.051, 0.038, 0.025],
                  [0.525, 0.19, 0.116, 0.072, 0.058, 0.044, 0.03],
                  [0.53, 0.20, 0.125, 0.080, 0.065, 0.05, 0.035]]


def ext_fifa_women_ranking(league, year, teams, list_of_matches, start_pts, match_factor, home_adv, num_of_matches,
                           rank_id, league_table, exp_id):
    """FIFA Women's ratings system (to be used externally - see next function to be used internally).

        Originally uses all historical match results data.
        Starting points according to when the first match was played or fixed 1000-1200-1500.
        Formulae:
            NewRating = OldRating + match_factor * (match_result_points_percentage - win_expectancy)
        see: http://www.fifa.com/worldranking/procedureandschedule/womenprocedure/

        Args:
            league: The league under investigation. (see: leagues.txt)
            year: The year (season) used for the rankings.
            teams: Dictionary of the participating teams in the league for given year.
            list_of_matches: Supplied list of matches data used for the rankings.
            start_pts: The initial points for each team. (default: 1000)
            match_factor: Match importance factor.
                60 for World Cup finals & Olympics;
                45 for World Cup & Olympics Qualifiers & Continental Finals;
                30 for Continental Qualifiers plus Friendly matches between top 10 teams;
                15 for friendly matches
            home_adv: Home advantage adjustment. (default: 100)
            num_of_matches: Total number of matches in our matches dataset.
            rank_id: The ranking id (used to identify it in DB for grading).
            league_table: The final league table (should be auto-generated).
            exp_id: The experiment id.

        Returns:
            A list demonstrating the FIFA Women's Rankings.
    """
    fifaw_teams = []
    for team in teams:
        fifaw_teams.append([team, start_pts])
    fifaw_teams.sort()

    for match in list_of_matches:
        home_goals = int(match[4])
        away_goals = int(match[5])
        goal_diff = abs(home_goals - away_goals)

        # Goals and goal difference correction
        if home_goals > 5:
            home_goals = 5
        if away_goals > 5:
            away_goals = 5
        if goal_diff > 6:
            goal_diff = 6

        home_team = -1
        away_team = -1
        for i in range(len(fifaw_teams)):
            if fifaw_teams[i][0] == match[2]:
                home_team = i
            elif fifaw_teams[i][0] == match[3]:
                away_team = i

        expected_home = 1 / (1 + 10 **
                             (-1 * (abs(fifaw_teams[home_team][1] - (fifaw_teams[away_team][1])) + home_adv) / 400))
        expected_away = 1 - expected_home
        if match[6][0][:1] == "H":
            fifaw_teams[home_team][1] += match_factor * (1 - goal_diff_perc[away_goals][goal_diff] - expected_home)
            fifaw_teams[away_team][1] += match_factor * (goal_diff_perc[away_goals][goal_diff] - expected_away)
        elif match[6][0][:1] == "A":
            fifaw_teams[home_team][1] += match_factor * (goal_diff_perc[home_goals][goal_diff] - expected_home)
            fifaw_teams[away_team][1] += match_factor * (1 - goal_diff_perc[home_goals][goal_diff] - expected_away)
        else:
            fifaw_teams[home_team][1] += match_factor * (goal_diff_perc[home_goals][goal_diff] - expected_home)
            fifaw_teams[away_team][1] += match_factor * (goal_diff_perc[away_goals][goal_diff] - expected_away)

    fifaw_teams.sort(key=lambda x: x[1])
    var = "ID: %d See list(table) in source for W-D-L pts\nVars: Match Factor: %d-HomeAdv: %d, NumOfMatches: %d (ext)"\
          % (rank_id, match_factor, home_adv, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, fifaw_teams[::-1])

    write_results_wid(fifaw_teams[::-1], "fifa_women_ranking", league, year, var, num_of_matches, grading, rank_id, exp_id)


def recursive_fifa_women_ranking(league, year, teams, list_of_matches, start_pts, match_factor, home_adv,
                                 num_of_matches, rank_id, league_table, exp_id):
    """FIFA Women's ratings system  - Recursive Ranking until no further change

        Args:
            Same as ext_ version

        Returns:
            A list demonstrating the FIFA Women's Rankings.
    """
    fifaw_teams = []
    for team in teams:
        fifaw_teams.append([team, start_pts])
    fifaw_teams.sort()

    for match in list_of_matches:
        home_goals = int(match[4])
        away_goals = int(match[5])
        goal_diff = abs(home_goals - away_goals)

        # Goals and goal difference correction
        if home_goals > 5:
            home_goals = 5
        if away_goals > 5:
            away_goals = 5
        if goal_diff > 6:
            goal_diff = 6

        home_team = -1
        away_team = -1
        for i in range(len(fifaw_teams)):
            if fifaw_teams[i][0] == match[2]:
                home_team = i
            elif fifaw_teams[i][0] == match[3]:
                away_team = i

        expected_home = 1 / (1 + 10 **
                             (-1 * (abs(fifaw_teams[home_team][1] - (fifaw_teams[away_team][1])) + home_adv) / 400))
        expected_away = 1 - expected_home
        if match[6][0][:1] == "H":
            fifaw_teams[home_team][1] += match_factor * (1 - goal_diff_perc[away_goals][goal_diff] - expected_home)
            fifaw_teams[away_team][1] += match_factor * (goal_diff_perc[away_goals][goal_diff] - expected_away)
        elif match[6][0][:1] == "A":
            fifaw_teams[home_team][1] += match_factor * (goal_diff_perc[home_goals][goal_diff] - expected_home)
            fifaw_teams[away_team][1] += match_factor * (1 - goal_diff_perc[home_goals][goal_diff] - expected_away)
        else:
            fifaw_teams[home_team][1] += match_factor * (goal_diff_perc[home_goals][goal_diff] - expected_home)
            fifaw_teams[away_team][1] += match_factor * (goal_diff_perc[away_goals][goal_diff] - expected_away)

    fifaw_teams.sort(key=lambda x: x[1])

    prev_ranking = []
    next_ranking = fifaw_teams[::-1]

    # while previous ranking is not the same with the re-ranking
    # we re-rank again until no changes occur in the ranking
    while rankings_uneven(prev_ranking, next_ranking):
        prev_ranking = next_ranking
        for match in list_of_matches:
            home_goals = int(match[4])
            away_goals = int(match[5])
            goal_diff = abs(home_goals - away_goals)

            if home_goals > 5:
                home_goals = 5
            if away_goals > 5:
                away_goals = 5
            if goal_diff > 6:
                goal_diff = 6

            home_team = -1
            away_team = -1
            for i in range(len(next_ranking)):
                if next_ranking[i][0] == match[2]:
                    home_team = i
                elif next_ranking[i][0] == match[3]:
                    away_team = i

            expected_home = 1 / (1 + 10 **
                                 (-1 * (abs(next_ranking[home_team][1] - (next_ranking[away_team][1])) + home_adv) / 400))
            expected_away = 1 - expected_home
            if match[6][0][:1] == "H":
                next_ranking[home_team][1] += match_factor * (1 - goal_diff_perc[away_goals][goal_diff] - expected_home)
                next_ranking[away_team][1] += match_factor * (goal_diff_perc[away_goals][goal_diff] - expected_away)
            elif match[6][0][:1] == "A":
                next_ranking[home_team][1] += match_factor * (goal_diff_perc[home_goals][goal_diff] - expected_home)
                next_ranking[away_team][1] += match_factor * (1 - goal_diff_perc[home_goals][goal_diff] - expected_away)
            else:
                next_ranking[home_team][1] += match_factor * (goal_diff_perc[home_goals][goal_diff] - expected_home)
                next_ranking[away_team][1] += match_factor * (goal_diff_perc[away_goals][goal_diff] - expected_away)

    next_ranking.sort(key=lambda x: x[1])

    var = "ID: %d See list(table) in source for W-D-L pts\nVars: Match Factor: %d-HomeAdv: %d, NumOfMatches: %d (rec)"\
          % (rank_id, match_factor, home_adv, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, next_ranking[::-1])

    write_results_wid(next_ranking[::-1], "fifa_women_ranking", league, year, var, num_of_matches,
                      grading, rank_id, exp_id)


def fifa_women_ranking(league, year, start_pts, match_factor, home_adv, random, rang):
    """Same as previous functions but only internal use (mainly for testing).
        random: Select random number of matches per team with upper bound rang. (default: False)
        rang: Total number of matches to select for each team or upper bound if random selection.
    """
    match_set_num = rang
    if random:
        match_set_num = randint(1, rang)

    teams = MySQLDriver.get_all_league_teams_for_year(league, year)
    fifaw_teams = []
    for team in teams:
        fifaw_teams.append([team, start_pts])
    fifaw_teams.sort()
    
    all_matches = set()

    for team in teams:
        team_matches = MySQLDriver.get_random_matches_for_team(team, league, year, match_set_num)
        for match in team_matches:
            all_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
        del team_matches[:]
    
    list_of_matches = list(all_matches)
    list_of_matches.sort()

    for match in list_of_matches:
        home_goals = int(match[4])
        away_goals = int(match[5])
        goal_diff = abs(home_goals - away_goals)

        # Goals and goal difference correction
        if home_goals > 5:
            home_goals = 5
        if away_goals > 5:
            away_goals = 5
        if goal_diff > 6:
            goal_diff = 6

        home_team = -1
        away_team = -1
        for i in range(len(fifaw_teams)):
            if fifaw_teams[i][0] == match[2]:
                home_team = i
            elif fifaw_teams[i][0] == match[3]:
                away_team = i

        expected_home = 1 / (1 + 10 **
                             (-1 * (abs(fifaw_teams[home_team][1] - (fifaw_teams[away_team][1])) + home_adv) / 400))
        expected_away = 1 - expected_home
        if match[6][0][:1] == "H":
            fifaw_teams[home_team][1] += match_factor * (1 - goal_diff_perc[away_goals][goal_diff] - expected_home)
            fifaw_teams[away_team][1] += match_factor * (goal_diff_perc[away_goals][goal_diff] - expected_away)
        elif match[6][0][:1] == "A":
            fifaw_teams[home_team][1] += match_factor * (goal_diff_perc[home_goals][goal_diff] - expected_home)
            fifaw_teams[away_team][1] += match_factor * (1 - goal_diff_perc[home_goals][goal_diff] - expected_away)
        else:
            fifaw_teams[home_team][1] += match_factor * (goal_diff_perc[home_goals][goal_diff] - expected_home)
            fifaw_teams[away_team][1] += match_factor * (goal_diff_perc[away_goals][goal_diff] - expected_away)

    fifaw_teams.sort(key=lambda x: x[1])
    var = "See list(table) in source for W-D-L pts\nVars:Random: %s - NumOfMatches: %d - Match Factor: %d-HomeAdv: %d "\
          % (random, rang, match_factor, home_adv)
    write_results(fifaw_teams[::-1], "fifa_women_ranking", league, year, var)


def main():
    fifa_women_ranking("E0", 2013, 1000, 24, 100, False, 38)


if __name__ == "__main__": main()