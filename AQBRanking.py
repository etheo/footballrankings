import MySQLDriver
from Helper import write_results
from Helper import write_results_wid
from Helper import rankings_uneven
from random import randint
from grading import RankingGrading

INITIAL_RATING = 1000
# TODO: EXPAND TO USE MORE THAN A SINGLE YEAR

aqb_teams = []


def team_rating_diff(home_team, away_team):
    """ Finds the rating difference between two given teams.

        Args:
            home_team: The Home team's name.
            away_team: The Away team's name.

        Returns:
            The Rating Difference of the given teams.
    """
    aqb_teams.sort(key=lambda x: x[1])  # sort on points
    home_team_rating = -1
    away_team_rating = -1
    for i in range(len(aqb_teams)):
        if aqb_teams[i][0] == home_team:
            home_team_rating = aqb_teams[i][1]
        if aqb_teams[i][0] == away_team:
            away_team_rating = aqb_teams[i][1]
    return home_team_rating - away_team_rating


def ext_aqb_ranking(league, year, teams, list_of_matches, win_pts, loss_pts, rating_diff_weight, equal_rate_adj,
                    rating_adj, rating_weight, home_adv, num_of_matches, rank_id, league_table, exp_id):
    """AQB Ratings System (to be used externally - see next function to be used internally).

        Active, Quality-Based ranking system (teams start with 1000 pts).
            Formulae:
                NewRating = OldRating + pts * (rating_adj + home_adv + (Loser Rating-Winner Rating) * rating_weight)
                OR in case of draw
                NewRating = OldRating + Rd * (rating_adj + home_adv + (HigherRatedTeam-LowerRatedTeam) * rating_weight)
        see: http://web.archive.org/web/20130206193228/http://image.co.nz/aqb

        Args:
            league: The league under investigation. (see: leagues.txt)
            year: The year (season) used for the rankings.
            teams: Dictionary of the participating teams in the league for given year.
            list_of_matches: Supplied list of matches data used for the rankings.
            win_pts: The win points factor. (default: 1)
            loss_pts: The loss points factor. (default: -1)
            rating_diff_weight: Draw weighting factor based on teams rating before the match. (default: 0.25)
            equal_rate_adj: Adjustment of the rating if teams were equally rated before the match. (default: 20)
            rating_adj: Base ratings change for any result. (default: 20)
            rating_weight: Weighting applied to the ratings difference. (default: 0.05)
            home_adv: Home advantage adjustment. (default: 4)
            num_of_matches: Total number of matches in our matches dataset.
            rank_id: The ranking id (used to identify it in DB for grading).
            league_table: The final league table (should be auto-generated).
            exp_id: The experiment id.

        Returns:
            A list demonstrating the AQB Ratings.
    """
    aqb_teams[:] = []

    for team in teams:
        aqb_teams.append([team, INITIAL_RATING])
    aqb_teams.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(aqb_teams)):
            if aqb_teams[i][0] == match[2]:
                home_team = i
            elif aqb_teams[i][0] == match[3]:
                away_team = i

        rating_diff = team_rating_diff(aqb_teams[home_team][0], aqb_teams[away_team][0])
        if match[6][0][:1] == "H":
            if abs(rating_diff) > 320:
                aqb_teams[home_team][1] += win_pts * (rating_adj - home_adv - rating_diff * rating_weight)
                aqb_teams[away_team][1] += loss_pts * (rating_adj - home_adv - rating_diff * rating_weight)
            elif rating_diff == 0:
                aqb_teams[home_team][1] += equal_rate_adj
                aqb_teams[away_team][1] -= equal_rate_adj
        elif match[6][0][:1] == "A":
            if rating_diff > 320:
                aqb_teams[home_team][1] += loss_pts * (rating_adj - home_adv + rating_diff * rating_weight)
                aqb_teams[away_team][1] += win_pts * (rating_adj - home_adv + rating_diff * rating_weight)
        else:
            if rating_diff > 0:
                aqb_teams[home_team][1] -= rating_diff_weight * (rating_adj - home_adv + rating_diff * rating_weight)
                aqb_teams[away_team][1] += rating_diff_weight * (rating_adj - home_adv + rating_diff * rating_weight)
            elif rating_diff < 0:
                aqb_teams[home_team][1] += rating_diff_weight * (rating_adj - home_adv - rating_diff * rating_weight)
                aqb_teams[away_team][1] -= rating_diff_weight * (rating_adj - home_adv - rating_diff * rating_weight)

    aqb_teams.sort(key=lambda x: x[1])
    var = "ID: %d Win Pts: %s, RD_Weight(Draw): %s, Loss Pts: %s\nVars: Rating Adj: %d, Rating Weight: %d-HomeAdv: %d,"\
          " NumOfMatches: %d (ext)" \
          % (rank_id, win_pts, rating_diff_weight, loss_pts, rating_adj, rating_weight, home_adv, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, aqb_teams[::-1])

    write_results_wid(aqb_teams[::-1], "aqb_ranking", league, year, var, num_of_matches, grading, rank_id, exp_id)


def recursive_aqb_ranking(league, year, teams, list_of_matches, win_pts, loss_pts, rating_diff_weight, equal_rate_adj,
                          rating_adj, rating_weight, home_adv, num_of_matches, rank_id, league_table, exp_id):
    """AQB Ratings System - Recursive Ranking until no further change

        Args:
            Same as ext_ version

        Returns:
            A list demonstrating the AQB Ratings.
    """
    aqb_teams[:] = []

    for team in teams:
        aqb_teams.append([team, INITIAL_RATING])
    aqb_teams.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(aqb_teams)):
            if aqb_teams[i][0] == match[2]:
                home_team = i
            elif aqb_teams[i][0] == match[3]:
                away_team = i

        rating_diff = team_rating_diff(aqb_teams[home_team][0], aqb_teams[away_team][0])
        if match[6][0][:1] == "H":
            if abs(rating_diff) > 320:
                aqb_teams[home_team][1] += win_pts * (rating_adj - home_adv - rating_diff * rating_weight)
                aqb_teams[away_team][1] += loss_pts * (rating_adj - home_adv - rating_diff * rating_weight)
            elif rating_diff == 0:
                aqb_teams[home_team][1] += equal_rate_adj
                aqb_teams[away_team][1] -= equal_rate_adj
        elif match[6][0][:1] == "A":
            if rating_diff > 320:
                aqb_teams[home_team][1] += loss_pts * (rating_adj - home_adv + rating_diff * rating_weight)
                aqb_teams[away_team][1] += win_pts * (rating_adj - home_adv + rating_diff * rating_weight)
        else:
            if rating_diff > 0:
                aqb_teams[home_team][1] -= rating_diff_weight * (rating_adj - home_adv + rating_diff * rating_weight)
                aqb_teams[away_team][1] += rating_diff_weight * (rating_adj - home_adv + rating_diff * rating_weight)
            elif rating_diff < 0:
                aqb_teams[home_team][1] += rating_diff_weight * (rating_adj - home_adv - rating_diff * rating_weight)
                aqb_teams[away_team][1] -= rating_diff_weight * (rating_adj - home_adv - rating_diff * rating_weight)

    aqb_teams.sort(key=lambda x: x[1])

    prev_ranking = []
    next_ranking = aqb_teams[::-1]

    # while previous ranking is not the same with the re-ranking
    # we re-rank again until no changes occur in the ranking
    while rankings_uneven(prev_ranking, next_ranking):
        prev_ranking = next_ranking
        for match in list_of_matches:
            home_team = -1
            away_team = -1
            for i in range(len(next_ranking)):
                if next_ranking[i][0] == match[2]:
                    home_team = i
                elif next_ranking[i][0] == match[3]:
                    away_team = i

            rating_diff = team_rating_diff(next_ranking[home_team][0], next_ranking[away_team][0])
            if match[6][0][:1] == "H":
                if abs(rating_diff) > 320:
                    next_ranking[home_team][1] += win_pts * (rating_adj - home_adv - rating_diff * rating_weight)
                    next_ranking[away_team][1] += loss_pts * (rating_adj - home_adv - rating_diff * rating_weight)
                elif rating_diff == 0:
                    next_ranking[home_team][1] += equal_rate_adj
                    next_ranking[away_team][1] -= equal_rate_adj
            elif match[6][0][:1] == "A":
                if rating_diff > 320:
                    next_ranking[home_team][1] += loss_pts * (rating_adj - home_adv + rating_diff * rating_weight)
                    next_ranking[away_team][1] += win_pts * (rating_adj - home_adv + rating_diff * rating_weight)
            else:
                if rating_diff > 0:
                    next_ranking[home_team][1] -= rating_diff_weight * (rating_adj - home_adv + rating_diff
                                                                        * rating_weight)
                    next_ranking[away_team][1] += rating_diff_weight * (rating_adj - home_adv + rating_diff
                                                                        * rating_weight)
                elif rating_diff < 0:
                    next_ranking[home_team][1] += rating_diff_weight * (rating_adj - home_adv - rating_diff
                                                                        * rating_weight)
                    next_ranking[away_team][1] -= rating_diff_weight * (rating_adj - home_adv - rating_diff
                                                                        * rating_weight)

    next_ranking.sort(key=lambda x: x[1])

    var = "ID: %d Win Pts: %s, RD_Weight(Draw): %s, Loss Pts: %s\nVars: Rating Adj: %d, Rating Weight: %d-HomeAdv: %d,"\
          " NumOfMatches: %d (rec)" \
          % (rank_id, win_pts, rating_diff_weight, loss_pts, rating_adj, rating_weight, home_adv, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, next_ranking[::-1])

    write_results_wid(next_ranking[::-1], "aqb_ranking", league, year, var, num_of_matches, grading, rank_id, exp_id)


def aqb_ranking(league, year, win_pts, loss_pts, rating_diff_weight, equal_rate_adj, rating_adj, rating_weight,
                home_adv, random, rang):
    """Same as previous functions but only internal use (mainly for testing).
        random: Select random number of matches per team with upper bound rang. (default: False)
        rang: Total number of matches to select for each team or upper bound if random selection.
    """
    match_set_num = rang
    if random:
        match_set_num = randint(1, rang)

    teams = MySQLDriver.get_all_league_teams_for_year(league, year)
    for team in teams:
        aqb_teams.append([team, 1000])
    aqb_teams.sort()
    
    all_matches = set()

    for team in teams:
        team_matches = MySQLDriver.get_random_matches_for_team(team, league, year, match_set_num)
        for match in team_matches:
            all_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
        del team_matches[:]
    
    list_of_matches = list(all_matches)
    list_of_matches.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(aqb_teams)):
            if aqb_teams[i][0] == match[2]:
                home_team = i
            elif aqb_teams[i][0] == match[3]:
                away_team = i

        rating_diff = team_rating_diff(aqb_teams[home_team][0], aqb_teams[away_team][0])
        if match[6][0][:1] == "H":
            if abs(rating_diff) <= 320:
                aqb_teams[home_team][1] += win_pts * (rating_adj - home_adv - rating_diff * rating_weight)
                aqb_teams[away_team][1] += loss_pts * (rating_adj - home_adv - rating_diff * rating_weight)
            elif rating_diff == 0:
                aqb_teams[home_team][1] += equal_rate_adj
                aqb_teams[away_team][1] -= equal_rate_adj
        elif match[6][0][:1] == "A":
                aqb_teams[home_team][1] += loss_pts * (rating_adj - home_adv + rating_diff * rating_weight)
                aqb_teams[away_team][1] += win_pts * (rating_adj - home_adv + rating_diff * rating_weight)
        else:
            if rating_diff > 0:
                aqb_teams[home_team][1] -= rating_diff_weight * (rating_adj - home_adv + rating_diff * rating_weight)
                aqb_teams[away_team][1] += rating_diff_weight * (rating_adj - home_adv + rating_diff * rating_weight)
            elif rating_diff < 0:
                aqb_teams[home_team][1] += rating_diff_weight * (rating_adj - home_adv - rating_diff * rating_weight)
                aqb_teams[away_team][1] -= rating_diff_weight * (rating_adj - home_adv - rating_diff * rating_weight)

    aqb_teams.sort(key=lambda x: x[1])
    var = "Win Pts: %s, RD_Weight(Draw): %s, Loss Pts: %s\nVars: Random: %s - NumOfMatches: %d-Rating Adj: %d," \
          "Rating Weight: %d-HomeAdv: %d "\
          % (win_pts, rating_diff_weight, loss_pts, random, rang, rating_adj, rating_weight, home_adv)
    write_results(aqb_teams[::-1], "aqb_ranking", league, year, var)


def main():
    aqb_ranking("E0", 2013, 1, -1, 0.25, 20, 20, 0.05, 4, False, 19)


if __name__ == "__main__": main()