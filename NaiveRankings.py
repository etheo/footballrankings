import MySQLDriver
from Helper import write_results
from random import shuffle

# Naive Rankings
# 1) Alphabetical
# 2) Random
# 3) TODO: Based on previous World Cups


# Alphabetical and Random rankings (self-explained)
def alpha_random(league, year):
    """Alphabetical and Random rankings (self-explained)

        Args:
            league: The league under investigation. (see: leagues.txt)
            year: The year (season) used for the rankings.
    """
    teams = []
    teams_set = MySQLDriver.get_all_league_teams_for_year(league, year)
    for team in teams_set:
        teams.append([team, 0])

    # Alphabetical sorted
    teams.sort()

    write_results(teams, "alphabetical_ranking", league, year, "")

    # Randomly shuffled
    shuffle(teams)

    write_results(teams, "random_ranking", league, year, "")


def main():
    alpha_random("E0", 2013)


if __name__ == "__main__": main()