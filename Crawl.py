import MySQLdb

db = MySQLdb.connect(host="localhost",
                     user="root",
                     passwd="1991",    # put DB password
                     db="football")

# Cursor object to execute queries.
cur = db.cursor()


def find_between(s, first, last):
    try:
        start = s.index(first) + len(first)
        end = s.index(last, start)
        return s[start:end]
    except ValueError:
        return ""


def place_of_the_match(city, home_country, away_country):
    sql_query = """SELECT x.country FROM world_info.country x
                   WHERE x.code IN (SELECT c.country from world_info.city c where c.city = '%s')""" % city.lower()
    cur.execute(sql_query)
    rows = cur.fetchall()

    for row in rows:
        origin = row[0]
        if str(home_country) == origin.rstrip():
            return 1
        elif away_country == origin.rstrip():
            return 2
    return 0


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def main():

    with open("C:/Users/serv32/Desktop/2014.htm", "r") as myfile:
        data = myfile.read().replace('\n', '')

    data = data.split('<tbody>', 1)[-1]
    data = data.split("</table>", 1)[0]

    #print data

    fname = "2014.txt"
    filename = "Z:/Football Results/%s" % fname

    counter = 1
    results = "div,date,HomeTeam,AwayTeam,FTHG,FTAG,FTR,Neutral,Place\n"

    with open(filename, "a") as f:
        f.write(results)

    while data != "</tbody>":
        if counter % 2 == 0:
            s = find_between(data, "<tr class=\"odd\">", "</tr>")

            # </span> Jordan</a></td>
            home = find_between(s, "</span>", "</a>")
            home = home.lstrip()

            # rowspan="2">2:1 (1:1)</td>
            score = find_between(s, "<td class=\"c bold twenty\" rowspan=\"2\">", "</td>")

            #report.html for confederations, index.html for WC
            if not is_number(score[:1]):
                score = find_between(score, "index.html\">", "</a>")

            score = score[:4]

            # index.html">Kuwait <span
            away = find_between(s, "<td class=\"r noBord thirty\">", " class")
            away = find_between(away, "\">", " <span")

            #print home + " - " + score + " - " + away

            data = data.replace("<tr class=\"odd\">" + s + "</tr>", "", 1)

            info = find_between(data, "<tr class=\"odd\">", "</tr>")

            #<td class="secInfo sixteen">01/01/2014</td>
            #<td class="secInfo sixteen">Doha  </td>
            #<td class="r secInfo thirty">Friendly</td>
            date = find_between(info, "<td class=\"secInfo sixteen\">", "</td>")
            place = find_between(info, "</td><td class=\"secInfo sixteen\">", "</td>")
            place = place.rstrip().lstrip()

            comp = find_between(info, "<td class=\"r secInfo thirty\">", "</td>")
            place = place.replace("'", "\\'")

            #print place + " - " + home + " - " + away

            neutral = place_of_the_match(place, home, away)
            goals = score.split(":")

            res = ""

            if int(goals[0]) > int(goals[1]):
                res = "H"
            elif int(goals[0]) == int(goals[1]):
                res = "D"
            else:
                res = "A"

            results = comp + "," + date + "," + home + "," + away + "," \
                       + goals[0] + "," + goals[1] + "," + res + ","\
                       + str(neutral) + "," + place + "\n"

            data = data.replace("<tr class=\"odd\">" + info + "</tr>", "", 1)

            counter += 1
        else:
            s = find_between(data, "<tr class=\"even\">", "</tr>")

            # </span> Jordan</a></td>
            home = find_between(s, "</span>", "</a>")
            home = home.lstrip()

            # rowspan="2">2:1 (1:1)</td>
            score = find_between(s, "<td class=\"c bold twenty\" rowspan=\"2\">", "</td>")

            #report.html for confederations, index.html for WC
            if not is_number(score[:1]):
                score = find_between(score, "index.html\">", "</a>")

            score = score[:4]

            # index.html">Kuwait <span
            away = find_between(s, "<td class=\"r noBord thirty\">", " class")
            away = find_between(away, "\">", " <span")

            #print home + " - " + score + " - " + away

            data = data.replace("<tr class=\"even\">" + s + "</tr>", "", 1)

            info = find_between(data, "<tr class=\"even\">", "</tr>")

            #<td class="secInfo sixteen">01/01/2014</td>
            #<td class="secInfo sixteen">Doha  </td>
            #<td class="r secInfo thirty">Friendly</td>
            date = find_between(info, "<td class=\"secInfo sixteen\">", "</td>")
            place = find_between(info, "</td><td class=\"secInfo sixteen\">", "</td>")
            place = place.rstrip().lstrip()

            comp = find_between(info, "<td class=\"r secInfo thirty\">", "</td>")

            place = place.replace("'", "\\'")

            #print place + " - " + home + " - " + away

            neutral = place_of_the_match(place, home, away)
            goals = score.split(":")

            res = ""

            if int(goals[0]) > int(goals[1]):
                res = "H"
            elif int(goals[0]) == int(goals[1]):
                res = "D"
            else:
                res = "A"

            results = comp + "," + date + "," + home + "," + away + "," \
                        + goals[0] + "," + goals[1] + "," + res + ","\
                        + str(neutral) + "," + place + "\n"

            data = data.replace("<tr class=\"even\">" + info + "</tr>", "", 1)

            counter += 1
        with open(filename, "a") as f:
            f.write(results)
    #with open(filename, "w+") as f:
    #   f.write(results)

if __name__ == "__main__": main()