#!/usr/bin/python
import MySQLdb
import warnings
from datetime import datetime


db = MySQLdb.connect(host="localhost",
                     user="root",    
                     passwd="",    # put DB password
                     db="football")

# Cursor object to execute queries.
cur = db.cursor()


def get_all_league_teams_for_year(league, year):
    """Get all league teams for a certain season.
        Args:
            league: given league (see leagues.txt)
            year: season eg 2013 for 2013-14 season
    """
    teams = {}
    sql_query = """SELECT DISTINCT HomeTeam FROM football.football_results
                WHERE `Div`='%s' AND (Date BETWEEN '%d/08/01' AND '%d/11/30')
                UNION
                SELECT DISTINCT AwayTeam FROM football.football_results
                WHERE `Div`='%s' AND (Date BETWEEN '%d/08/01' AND '%d/11/30') ORDER BY HomeTeam""" \
                % (league, year, year, league, year, year)
    cur.execute(sql_query)
    rows = cur.fetchall()
    for row in rows:
        home_team = row[0]
        if home_team not in teams:
            teams[home_team] = 0
    return teams


matches = []


# TODO: Get matches from more than 1 year
# TODO: Partition year into periods (4 periods to visualise FIFA rankings)
def get_random_matches_for_team(team, div, year, number):
    get_matches = """SELECT * FROM football.football_results
                  WHERE (`HomeTeam` = "%s" OR `AwayTeam` = "%s")
                  AND `Div`='%s' AND (Date BETWEEN '%d/07/20' AND '%d/06/30')
                  ORDER BY RAND() LIMIT %d""" % (team, team, div, year, year+1, number)
    cur.execute(get_matches)
    rows = cur.fetchall()
    for row in rows:
        matches.append([row[1], row[2], row[3], row[4], row[5], row[6], row[7]])
    return matches


def store_ranking_grading(algorithm, manhattan_dist, hamming_dist, lee_dist, spearman_rho, kendall_tau, gamma):
    """DEPRECATED (Use insert_ranking instead)
        Store the grading of a ranking in DB.
        Self-explained parameters.
    """
    warnings.warn("deprecated", DeprecationWarning)
    try:
        insert_grading = """INSERT INTO ranking_gradings
                            (algorithm, manhattan, hamming, lee, spearman_rho, kendall_tau, goodman_gamma)
                            VALUES (\'%s\', %f, %f, %f, %f, %f, %f)""" \
                            % (algorithm, manhattan_dist, hamming_dist, lee_dist, spearman_rho, kendall_tau, gamma)
        cur.execute(insert_grading)
        db.commit()
    except MySQLdb:
        db.rollback()


def get_next_ranking_id():
    """
        Get next Auto_Increment ID for ranking table.
    """
    get_next_id_query = """SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = "ranking"
                        AND table_schema = DATABASE()"""

    cur.execute(get_next_id_query)
    rows = cur.fetchall()
    ai_id = -1
    for row in rows:
        ai_id = row[0]
    return ai_id


def insert_ranking(ranking, algorithm, division, year, var, num_of_matches, manhattan_dist, hamming_dist, lee_dist,
                   spearman_rho, kendall_tau, gamma, rank_id, exp_id):
    """Insert Ranking results into the Database.

        Args:
            ranking: The actual (ordered list) ranking.
            alg: Name of the algorithm used.
            div: League/Division under investigation.
            year: Season under investigation.
            var: String variable containing additional information about this ranking execution.
            rank_id: The unique ID of this ranking execution.
    """
    try:
        insert_ranking = """INSERT INTO ranking
                            (id, algorithm, information, league, year, ranking_list, num_matches, manhattan, hamming,
                            lee, spearman_rho, kendall_tau, goodman_gamma, experiment_id)
                            VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \"%r\", %d, %f, %f, %f, %f, %f, %f, %d)""" \
                            % (rank_id, algorithm, var, division, year, tuple(ranking), num_of_matches,
                               manhattan_dist, hamming_dist, lee_dist, spearman_rho, kendall_tau, gamma, exp_id)
        cur.execute(insert_ranking)
        db.commit()
    except MySQLdb:
        db.rollback()


def get_next_experiment_id():
    """
        Get next Auto_Increment ID for experiment table.
    """
    get_next_id_query = """SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = "experiment"
                        AND table_schema = DATABASE()"""

    cur.execute(get_next_id_query)
    rows = cur.fetchall()
    ai_id = -1
    for row in rows:
        ai_id = row[0]
    return ai_id


def insert_experiment():
    """
        Insert new Experiment into the Database
    """
    try:
        insert_exp = """INSERT INTO experiment (end_datetime) VALUES (NULL)"""
        cur.execute(insert_exp)
        db.commit()
    except MySQLdb:
        db.rollback()


def update_experiment(exp_id):
    """
        Insert new Experiment into the Database
    """
    try:
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        update_exp = """UPDATE experiment SET end_datetime = \"%s\" WHERE id = %d""" % (now, exp_id)
        cur.execute(update_exp)
        db.commit()
    except MySQLdb:
        db.rollback()

def main():
    get_all_league_teams_for_year("SP1", 2001)


if __name__ == "__main__": main()