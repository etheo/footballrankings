import MySQLdb

db1 = MySQLdb.connect(host="localhost",
                      user="root",
                      passwd="1991",    # put DB password
                      db="football")

# Cursor object to execute queries.
cur = db1.cursor()


def get_result_stats():

    #leagues = ["E0"]
    algs = ["fifa_ranking", "fifa_women_ranking", "elo_ranking", "elephant_ranking",
            "elephant_rankingS", "pagerank", "aqb_ranking"]

    for alg in algs:
        results = "%s\n" % alg
        results += "%s\t%s\t%s\t%s\t%s\n" % ("Size".rjust(5), "Rho".rjust(5), "Tau".rjust(5),
                                             "Man.".rjust(6), "Ham.".rjust(5))

        for size in range(4, 21):
            results += "%d\n" % size
            query = """select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 23 and num_matches = %d and algorithm = '%s'
                       union all
                       select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 24 and num_matches = %d and algorithm = '%s'
                       union all
                       select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 25 and num_matches = %d and algorithm = '%s'
                       union all
                       select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 26 and num_matches = %d and algorithm = '%s'""" \
                    % (size, alg, size, alg, size, alg, size, alg)
            cur.execute(query)
            rows = cur.fetchall()
            rho = 0
            tau = 0
            manhattan = 0
            hamming = 0
            for row in rows:
                rho += row[0]
                tau += row[1]
                manhattan += row[2]
                hamming += row[3]
                results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(row[0]), 2))).ljust(5),
                                                 str(format(round(float(row[1]), 2))).ljust(5),
                                                 str(format(round(float(row[2]), 2))).ljust(6),
                                                 str(format(round(float(row[3]), 2))).ljust(5))
            results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(rho) / 4, 2))).ljust(5),
                                             str(format(round(float(tau) / 4, 2))).ljust(5),
                                             str(format(round(float(manhattan) / 4, 2))).ljust(5),
                                             str(format(round(float(hamming) / 4, 2))).ljust(5))
        fname = "stats_%s.txt" % alg
        filename = "Z:/Football Results/%s" % fname

        with open(filename, "w+") as f:
            f.write(results)


def get_result_stats_small():

    #leagues = ["E0"]
    algs = ["fifa_ranking", "fifa_women_ranking", "elo_ranking", "elephant_ranking",
            "elephant_rankingS", "pagerank", "aqb_ranking"]

    for alg in algs:
        results = "%s\n" % alg
        results += "%s\t%s\t%s\t%s\t%s\n" % ("Size".rjust(5), "Rho".rjust(5), "Tau".rjust(5),
                                             "Man.".rjust(6), "Ham.".rjust(5))

        for size in range(1, 4):
            results += "%d\n" % size
            query = """select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 43 and num_matches = %d and algorithm = '%s'
                       union all
                       select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 44 and num_matches = %d and algorithm = '%s'""" \
                    % (size, alg, size, alg)
            cur.execute(query)
            rows = cur.fetchall()
            rho = 0
            tau = 0
            manhattan = 0
            hamming = 0
            for row in rows:
                rho += row[0]
                tau += row[1]
                manhattan += row[2]
                hamming += row[3]
                results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(row[0]), 2))).ljust(5),
                                                 str(format(round(float(row[1]), 2))).ljust(5),
                                                 str(format(round(float(row[2]), 2))).ljust(6),
                                                 str(format(round(float(row[3]), 2))).ljust(5))
            results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(rho) / 2, 2))).ljust(5),
                                             str(format(round(float(tau) / 2, 2))).ljust(5),
                                             str(format(round(float(manhattan) / 2, 2))).ljust(5),
                                             str(format(round(float(hamming) / 2, 2))).ljust(5))
        fname = "stats_small_%s.txt" % alg
        filename = "Z:/Football Results/%s" % fname

        with open(filename, "w+") as f:
            f.write(results)


def get_result_stats_huge():

    #leagues = ["E0"]
    algs = ["fifa_ranking", "fifa_women_ranking", "elo_ranking", "elephant_ranking",
            "elephant_rankingS", "pagerank", "aqb_ranking"]

    for alg in algs:
        results = "%s\n" % alg
        results += "%s\t%s\t%s\t%s\t%s\n" % ("Size".rjust(5), "Rho".rjust(5), "Tau".rjust(5),
                                             "Man.".rjust(6), "Ham.".rjust(5))

        for size in range(21, 46):
            results += "%d\n" % size
            query = """select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 42 and num_matches = %d and algorithm = '%s'""" \
                    % (size, alg)
            cur.execute(query)
            rows = cur.fetchall()
            rho = 0
            tau = 0
            manhattan = 0
            hamming = 0
            for row in rows:
                rho += row[0]
                tau += row[1]
                manhattan += row[2]
                hamming += row[3]
                results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(row[0]), 2))).ljust(5),
                                                 str(format(round(float(row[1]), 2))).ljust(5),
                                                 str(format(round(float(row[2]), 2))).ljust(6),
                                                 str(format(round(float(row[3]), 2))).ljust(5))
            results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(rho) / 1, 2))).ljust(5),
                                             str(format(round(float(tau) / 1, 2))).ljust(5),
                                             str(format(round(float(manhattan) / 1, 2))).ljust(5),
                                             str(format(round(float(hamming) / 1, 2))).ljust(5))
        fname = "stats_huge_%s.txt" % alg
        filename = "Z:/Football Results/%s" % fname

        with open(filename, "w+") as f:
            f.write(results)


def get_result_stats_pagerank():

    #leagues = ["E0"]
    algs = ["1e-05", "0.85"]

    for alg in algs:
        results = "%s\n" % alg
        results += "%s\t%s\t%s\t%s\t%s\n" % ("Size".rjust(5), "Rho".rjust(5), "Tau".rjust(5),
                                             "Man.".rjust(6), "Ham.".rjust(5))

        for size in range(1, 39):
            results += "%d\n" % size
            query = """select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 45 and num_matches = %d and information LIKE '%%%s%%'
                       union all
                       select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 47 and num_matches = %d and information LIKE '%%%s%%' """ \
                    % (size, alg, size, alg)
            cur.execute(query)
            rows = cur.fetchall()

            rho = 0
            tau = 0
            manhattan = 0
            hamming = 0
            for row in rows:
                rho += row[0]
                tau += row[1]
                manhattan += row[2]
                hamming += row[3]
                results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(row[0]), 2))).ljust(5),
                                                 str(format(round(float(row[1]), 2))).ljust(5),
                                                 str(format(round(float(row[2]), 2))).ljust(6),
                                                 str(format(round(float(row[3]), 2))).ljust(5))
            results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(rho) / 2, 2))).ljust(5),
                                             str(format(round(float(tau) / 2, 2))).ljust(5),
                                             str(format(round(float(manhattan) / 2, 2))).ljust(5),
                                             str(format(round(float(hamming) / 2, 2))).ljust(5))
        fname = "stats_small_%s.txt" % alg
        filename = "Z:/Football Results/%s" % fname

        with open(filename, "w+") as f:
            f.write(results)


def get_result_stats_pagerank_vs():

    #leagues = ["E0"]
    algs = ["1e-05", "0.85"]
    maxalgs = ["spearman_rho", "kendall_tau"]
    minalgs = ["manhattan", "hamming"]
    exps = [45, 47]
    results = "pagerank 1e-05 vs 0.85\n"
    for exp in exps:
        results += "%d\n" % exp
        results += "%s\t%s\t%s\t%s\t%s\n" % ("Size".rjust(5), "Rho".rjust(5), "Tau".rjust(5),
                                             "Man.".rjust(6), "Ham.".rjust(5))

        for size in range(1, 39):
            results += "%d\n" % size
            for alg in maxalgs:
                alg1best = 0
                alg2best = 0
                results += "%s\t" % alg
                query = """SELECT x.information FROM ranking x
                JOIN ( SELECT league , year , MAX(%s) max_value, experiment_id, num_matches FROM ranking
                where experiment_id = %d and num_matches = %d
                GROUP BY league , year ) y
                ON y.league = x.league AND y.year = x.year AND y.max_value = x.%s AND y.experiment_id = x.experiment_id
                AND x.num_matches = y.num_matches""" % (alg, exp, size, alg)
                cur.execute(query)
                rows = cur.fetchall()

                for row in rows:
                    if algs[0] in row[0]:
                        alg1best += 1
                    else:
                        alg2best += 1
                results += "%d-%d\t" % (alg1best, alg2best)

            for alg in minalgs:
                alg1best = 0
                alg2best = 0
                results += "%s\t" % alg
                query = """SELECT x.information FROM ranking x
                JOIN ( SELECT league , year , MIN(%s) max_value, experiment_id, num_matches FROM ranking
                where experiment_id = %d and num_matches = %d
                GROUP BY league , year ) y
                ON y.league = x.league AND y.year = x.year AND y.max_value = x.%s AND y.experiment_id = x.experiment_id
                AND x.num_matches = y.num_matches""" % (alg, exp, size, alg)
                cur.execute(query)
                rows = cur.fetchall()

                for row in rows:
                    if algs[0] in row[0]:
                        alg1best += 1
                    else:
                        alg2best += 1
                results += "%d-%d\t" % (alg1best, alg2best)
            results += "\n"
        results += "\n"
    fname = "stats_win_pagerank.txt"
    filename = "Z:/Football Results/%s" % fname

    with open(filename, "w+") as f:
        f.write(results)


def get_result_stats_elephant():

    #leagues = ["E0"]
    algs = ["elephant_rankingS", "elephant_ranking"]

    for alg in algs:
        results = "%s\n" % alg
        results += "%s\t%s\t%s\t%s\t%s\n" % ("Size".rjust(5), "Rho".rjust(5), "Tau".rjust(5),
                                             "Man.".rjust(6), "Ham.".rjust(5))

        for size in range(1, 39):
            results += "%d\n" % size
            query = """select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       from ranking
                       where experiment_id = 49 and num_matches = %d and algorithm = '%s'""" % (size, alg)
                       #union all
                       #select avg(spearman_rho), avg(kendall_tau), avg(manhattan), avg(hamming)
                       #from ranking
                       #where experiment_id = 50 and num_matches = %d and algorithm = '%s' """ \
                    #% (size, alg, size, alg)
            cur.execute(query)
            rows = cur.fetchall()

            rho = 0
            tau = 0
            manhattan = 0
            hamming = 0
            for row in rows:
                rho += row[0]
                tau += row[1]
                manhattan += row[2]
                hamming += row[3]
                results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(row[0]), 2))).ljust(5),
                                                 str(format(round(float(row[1]), 2))).ljust(5),
                                                 str(format(round(float(row[2]), 2))).ljust(6),
                                                 str(format(round(float(row[3]), 2))).ljust(5))
            results += "%s\t%s\t%s\t%s\n" % (str(format(round(float(rho) / 2, 2))).ljust(5),
                                             str(format(round(float(tau) / 2, 2))).ljust(5),
                                             str(format(round(float(manhattan) / 2, 2))).ljust(5),
                                             str(format(round(float(hamming) / 2, 2))).ljust(5))
        fname = "stats_small_%s.txt" % alg
        filename = "Z:/Football Results/%s" % fname

        with open(filename, "w+") as f:
            f.write(results)


def get_result_stats_elephant_vs():

    #leagues = ["E0"]
    algs = ["elephant_rankingS", "elephant_ranking"]
    maxalgs = ["spearman_rho", "kendall_tau"]
    minalgs = ["manhattan", "hamming"]
    exps = [49]
    results = "elescaled vs ele\n"
    for exp in exps:
        results += "%d\n" % exp
        results += "%s\t%s\t%s\t%s\t%s\n" % ("Size".rjust(5), "Rho".rjust(5), "Tau".rjust(5),
                                             "Man.".rjust(6), "Ham.".rjust(5))

        for size in range(1, 39):
            results += "%d\n" % size
            for alg in maxalgs:
                eles_best = 0
                ele_best = 0
                results += "%s\t" % alg
                query = """SELECT x.algorithm FROM ranking x
                JOIN ( SELECT league , year , MAX(%s) max_value, experiment_id, num_matches FROM ranking
                where experiment_id = %d and num_matches = %d
                GROUP BY league , year ) y
                ON y.league = x.league AND y.year = x.year AND y.max_value = x.%s AND y.experiment_id = x.experiment_id
                AND x.num_matches = y.num_matches""" % (alg, exp, size, alg)
                cur.execute(query)
                rows = cur.fetchall()

                for row in rows:
                    if algs[0] == row[0]:
                        eles_best += 1
                    else:
                        ele_best += 1
                results += "%d-%d\t" % (eles_best, ele_best)

            for alg in minalgs:
                eles_best = 0
                ele_best = 0
                results += "%s\t" % alg
                query = """SELECT x.algorithm FROM ranking x
                JOIN ( SELECT league , year , MIN(%s) max_value, experiment_id, num_matches FROM ranking
                where experiment_id = %d and num_matches = %d
                GROUP BY league , year ) y
                ON y.league = x.league AND y.year = x.year AND y.max_value = x.%s AND y.experiment_id = x.experiment_id
                AND x.num_matches = y.num_matches""" % (alg, exp, size, alg)
                cur.execute(query)
                rows = cur.fetchall()

                for row in rows:
                    if algs[0] == row[0]:
                        eles_best += 1
                    else:
                        ele_best += 1
                results += "%d-%d\t" % (eles_best, ele_best)
            results += "\n"
        results += "\n"
    fname = "stats_win_elephant.txt"
    filename = "Z:/Football Results/%s" % fname

    with open(filename, "w+") as f:
        f.write(results)


def main():
    #get_result_stats()
    #get_result_stats_small()
    #get_result_stats_huge()
    #get_result_stats_pagerank()
    #get_result_stats_pagerank_vs()
    get_result_stats_elephant()
    get_result_stats_elephant_vs()

if __name__ == "__main__": main()