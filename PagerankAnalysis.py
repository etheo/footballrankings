import SimpleRanking
import PageRanking
import MySQLDriver


def main():
    """
        Experimental function that demonstrates how to externally use
        the ranking algorithms to automatise the experiments.

        PRODUCE_FILES: default True
            If you do not wish to generate a file for each ranking set PRODUCE_FILES to False
    """
    #leagues = ["E0"]
    leagues = ["E0", "E1", "D1", "D2", "SP1", "SP2", "I1", "I2",
               "P1", "G1", "T1", "F1", "F2", "B1", "SC0", "SC1", "N1"]
    #year = 1994
    #match_set_num = 19
    damping_factors = [0.00001, 0.85]

    exp_id = MySQLDriver.get_next_experiment_id()
    MySQLDriver.insert_experiment()

    for league in leagues:
        for year in range(1993, 2014):
            teams = MySQLDriver.get_all_league_teams_for_year(league, year)
            if teams:
                season_matches = set()
                total_team_matches = 0
                for team in teams:
                    team_season_matches = MySQLDriver.get_random_matches_for_team(team, league, year, 52)
                    for match in team_season_matches:
                        season_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
                    if len(season_matches) > total_team_matches:
                        total_team_matches = len(season_matches)
                    del team_season_matches[:]

                list_all_matches = list(season_matches)
                list_all_matches.sort()
                # Generate final league table.
                league_table = SimpleRanking.generate_league_standings(league, year, teams, list_all_matches, 3, 1, 0,
                                                                       0, 80, 999, exp_id)

                for match_set_num in range(1, 39):
                    if match_set_num <= total_team_matches:
                        subset_matches = set()
                        for team in teams:
                            team_sub_matches = MySQLDriver.get_random_matches_for_team(team, league, year, match_set_num)
                            for match in team_sub_matches:
                                subset_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
                            del team_sub_matches[:]

                        list_of_matches = list(subset_matches)
                        list_of_matches.sort()

                        #simple_ranking = SimpleRanking.generate_league_standings(league, year, teams, list_of_matches,
                        # 3, 2, 1, 0, match_set_num)

                        # PageRank-ing
                        for value in damping_factors:
                            next_id = MySQLDriver.get_next_ranking_id()
                            PageRanking.ext_page_ranking(league, year, teams, list_of_matches, 20, value, match_set_num,
                                                         next_id, league_table, exp_id)

    MySQLDriver.update_experiment(exp_id)

if __name__ == "__main__": main()