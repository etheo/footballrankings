import MySQLDriver
from Helper import write_results
from Helper import write_results_wid
from Helper import rankings_uneven
from random import randint
from grading import RankingGrading

# TODO: Divide league into "confederations" to visualize actual FIFA confederations (add confederation coefficient)
# TODO: Match Importance factor
fifa_teams = []


def team_strength(team):
    """Determine a given team's strength.

        Args:
            team: Team's name.

        Returns:
            The team's strength.
    """
    fifa_teams.sort(key=lambda x: x[1])  # sort on points
    team_rank = -1
    for i in range(len(fifa_teams)):
        if fifa_teams[i][0] == team:
            team_rank = i
    return fifa_teams.__sizeof__() if team_rank == 0 else fifa_teams.__sizeof__() - team_rank


def ext_fifa_ranking(league, year, teams, list_of_matches, win_pts, draw_pts, loss_pts, match_factor, home_adv,
                     num_of_matches, rank_id, league_table, exp_id):
    """FIFA World Ranking System (to be used externally - see next function to be used internally).

        Teams start with 0 points
        OR teams start with a rating from a certain year (2006)
        OR experiment more.
        Uses matches up to 4 years ago with scaled importance (20-30-50-100 %).
        Formulae:
            Earned_Points = match_result *  match_factor * team_strength(opponent) * confederation_coeff
        see: http://www.fifa.com/worldranking/procedureandschedule/menprocedure/index.html

        Args:
            league: The league under investigation. (see: leagues.txt)
            year: The year (season) used for the rankings.
            teams: Dictionary of the participating teams in the league for given year.
            list_of_matches: Supplied list of matches data used for the rankings
            win_pts: The win points variable. (default: 3)
            draw_pts: The draw points variable. (default: 1)
            loss_pts: The loss points variable. (default: 0)
            match_factor: Match importance factor (I).
                -Friendly match (including small competitions): I = 1.0
                -FIFA World Cup qualifier or confederation-level qualifier: I = 2.5
                -Confederation-level final competition or FIFA Confederations Cup: I = 3.0
                -FIFA World Cup final competition: I = 4.0
            home_adv: Home advantage adjustment. (custom default: 0)
            num_of_matches: Total number of matches in our matches dataset.
            rank_id: The ranking id (used to identify it in DB for grading).
            league_table: The final league table (should be auto-generated).
            exp_id: The experiment id.

        Returns:
            A list demonstrating the FIFA Rankings.
    """
    fifa_teams[:] = []

    for team in teams:
        fifa_teams.append([team, 0])
    fifa_teams.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(fifa_teams)):
            if fifa_teams[i][0] == match[2]:
                home_team = i
            elif fifa_teams[i][0] == match[3]:
                away_team = i

        if match[6][0][:1] == "H":
            fifa_teams[home_team][1] += win_pts * match_factor * team_strength(fifa_teams[away_team][0])
        elif match[6][0][:1] == "A":
            fifa_teams[away_team][1] += win_pts * match_factor * team_strength(fifa_teams[home_team][0])
        else:
            fifa_teams[home_team][1] += draw_pts * match_factor * team_strength(fifa_teams[away_team][0])
            fifa_teams[away_team][1] += draw_pts * match_factor * team_strength(fifa_teams[home_team][0])

    fifa_teams.sort(key=lambda x: x[1])
    var = "ID: %d Win Pts: %d, Draw Pts: %d, Loss Pts: %d\nVars: Match Factor: %d-HomeAdv: %d, NumOfMatches: %d (ext)"\
          % (rank_id, win_pts, draw_pts, loss_pts, match_factor, home_adv, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, fifa_teams[::-1])

    write_results_wid(fifa_teams[::-1], "fifa_ranking", league, year, var, num_of_matches, grading, rank_id, exp_id)


def recursive_fifa_ranking(league, year, teams, list_of_matches, win_pts, draw_pts, loss_pts, match_factor, home_adv,
                           num_of_matches, rank_id, league_table, exp_id):
    """FIFA World Ranking System  - Recursive Ranking until no further change

        Args:
            Same as ext_ version

        Returns:
            A list demonstrating the FIFA Rankings.
    """
    fifa_teams[:] = []

    for team in teams:
        fifa_teams.append([team, 0])
    fifa_teams.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(fifa_teams)):
            if fifa_teams[i][0] == match[2]:
                home_team = i
            elif fifa_teams[i][0] == match[3]:
                away_team = i

        if match[6][0][:1] == "H":
            fifa_teams[home_team][1] += win_pts * match_factor * team_strength(fifa_teams[away_team][0])
        elif match[6][0][:1] == "A":
            fifa_teams[away_team][1] += win_pts * match_factor * team_strength(fifa_teams[home_team][0])
        else:
            fifa_teams[home_team][1] += draw_pts * match_factor * team_strength(fifa_teams[away_team][0])
            fifa_teams[away_team][1] += draw_pts * match_factor * team_strength(fifa_teams[home_team][0])

    fifa_teams.sort(key=lambda x: x[1])

    prev_ranking = []
    next_ranking = fifa_teams[::-1]

    # while previous ranking is not the same with the re-ranking
    # we re-rank again until no changes occur in the ranking
    while rankings_uneven(prev_ranking, next_ranking):
        prev_ranking = next_ranking
        for match in list_of_matches:
            home_team = -1
            away_team = -1
            for i in range(len(next_ranking)):
                if next_ranking[i][0] == match[2]:
                    home_team = i
                elif next_ranking[i][0] == match[3]:
                    away_team = i

            if match[6][0][:1] == "H":
                next_ranking[home_team][1] += win_pts * match_factor * team_strength(next_ranking[away_team][0])
            elif match[6][0][:1] == "A":
                next_ranking[away_team][1] += win_pts * match_factor * team_strength(next_ranking[home_team][0])
            else:
                next_ranking[home_team][1] += draw_pts * match_factor * team_strength(next_ranking[away_team][0])
                next_ranking[away_team][1] += draw_pts * match_factor * team_strength(next_ranking[home_team][0])

    next_ranking.sort(key=lambda x: x[1])

    var = "ID: %d Win Pts: %d, Draw Pts: %d, Loss Pts: %d\nVars: Match Factor: %d-HomeAdv: %d, NumOfMatches: %d (rec)"\
          % (rank_id, win_pts, draw_pts, loss_pts, match_factor, home_adv, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, next_ranking[::-1])

    write_results_wid(next_ranking[::-1], "fifa_ranking", league, year, var, num_of_matches, grading, rank_id, exp_id)


def fifa_ranking(league, year, win_pts, draw_pts, loss_pts, match_factor, home_adv, random, rang):
    """Same as previous functions but only internal use (mainly for testing).
        random: Select random number of matches per team with upper bound rang. (default: False)
        rang: Total number of matches to select for each team or upper bound if random selection.
    """
    match_set_num = rang
    if random:
        match_set_num = randint(1, rang)

    teams = MySQLDriver.get_all_league_teams_for_year(league, year)
    for team in teams:
        fifa_teams.append([team, 0])
    fifa_teams.sort()
    
    all_matches = set()

    for team in teams:
        team_matches = MySQLDriver.get_random_matches_for_team(team, league, year, match_set_num)
        for match in team_matches:
            all_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
        del team_matches[:]
    
    list_of_matches = list(all_matches)
    list_of_matches.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(fifa_teams)):
            if fifa_teams[i][0] == match[2]:
                home_team = i
            elif fifa_teams[i][0] == match[3]:
                away_team = i

        if match[6][0][:1] == "H":
            fifa_teams[home_team][1] += win_pts * match_factor * team_strength(fifa_teams[away_team][0]) * match_factor
        elif match[6][0][:1] == "A":
            fifa_teams[away_team][1] += win_pts * match_factor * team_strength(fifa_teams[home_team][0]) * match_factor
        else:
            fifa_teams[home_team][1] += draw_pts * match_factor * team_strength(fifa_teams[away_team][0]) * match_factor
            fifa_teams[away_team][1] += draw_pts * match_factor * team_strength(fifa_teams[home_team][0]) * match_factor

    fifa_teams.sort(key=lambda x: x[1])
    var = "Win Pts: %d, Draw Pts: %d, Loss Pts: %d\nVars: Random: %s - NumOfMatches: %d-Match Factor: %d-HomeAdv: %d "\
          % (win_pts, draw_pts, loss_pts, random, rang, match_factor, home_adv)
    write_results(fifa_teams[::-1], "fifa_ranking", league, year, var)


def main():
    fifa_ranking("E0", 2013, 3, 1, 0, 1, 0, False, 19)


if __name__ == "__main__": main()