import MySQLdb

db1 = MySQLdb.connect(host="localhost",
                      user="root",
                      passwd="1991",    # put DB password
                      db="football")

# Cursor object to execute queries.
cur = db1.cursor()


def get_result_stats():

    #leagues = ["E0"]
    leagues = ["E0", "E1", "D1", "D2", "SP1", "SP2", "I1", "I2",
               "P1", "G1", "T1", "F1", "F2", "B1", "SC0", "SC1", "N1"]

    for league in leagues:
        results = "%s\n" % league
        results += "    \t%s\t%s\t%s\t%s\n" % ("HomeW".rjust(16), "Draw".rjust(16), "AwayW".rjust(16), "Total".rjust(16))
        total_home_wins = 0
        total_draws = 0
        total_away_wins = 0
        total_matches = 0
        for year in range(1993, 2014):
            results += "%d\t" % year
            query = """select count(*) from football_results
                        where ftr LIKE "%%H%%" and `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'
                        union all
                        select count(*) from football_results
                        where ftr LIKE "%%D%%" and `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'
                        union all
                        select count(*) from football_results
                        where ftr LIKE "%%A%%" and `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'
                        union all
                        select count(*) from football_results
                        where `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'"""\
                    % (league, year, year+1, league, year, year+1, league, year, year+1, league, year, year+1)
            cur.execute(query)
            rows = cur.fetchall()
            home_wins = 0
            draws = 0
            away_wins = 0
            matches = 0
            counter = 0
            for row in rows:
                if counter == 0:
                    home_wins = row[0]
                elif counter == 1:
                    draws = row[0]
                elif counter == 2:
                    away_wins = row[0]
                else:
                    matches = row[0]
                counter += 1

            if home_wins > 0 and draws > 0 and away_wins > 0 and matches > 0:
                results += "%s\t" % \
                           (str(home_wins).ljust(5) + " (" +
                            str(format(round(float(home_wins)/matches * 100, 2))) + "%)").ljust(16)
                total_home_wins += home_wins
                results += "%s\t" % \
                           (str(draws).ljust(5) + " (" +
                            str(format(round(float(draws)/matches * 100, 2))) + "%)").ljust(16)
                total_draws += draws
                results += "%s\t" % \
                           (str(away_wins).ljust(5) + " (" +
                            str(format(round(float(away_wins)/matches * 100, 2))) + "%)").ljust(16)
                total_away_wins += away_wins
                results += "%s\t" % \
                           (str(matches).ljust(5) + " (" +
                            str(format(round(float(matches)/matches * 100, 2))) + "%)").ljust(16)
                total_matches += matches
                results += "\n"
        results += "%s\t%s\t%s\t%s\t%s" \
                   % ("tot.",
                      (str(total_home_wins).ljust(5) + " (" +
                      str(format(round(float(total_home_wins)/total_matches*100, 2))) + "%)").ljust(16),
                      (str(total_draws).ljust(5) + " (" +
                      str(format(round(float(total_draws)/total_matches*100, 2))) + "%)").ljust(16),
                      (str(total_away_wins).ljust(5) + " (" +
                      str(format(round(float(total_away_wins)/total_matches*100, 2))) + "%)").ljust(16),
                      (str(total_matches).ljust(5) + " (" +
                      str(format(round(float(total_matches)/total_matches*100, 2))) + "%)").ljust(16))

        results += "\n\nGoal Stats\n\n%s" % get_goal_stats(league)

        fname = "results_%s.txt" % league
        filename = "Z:/Football Results/%s" % fname

        with open(filename, "w+") as f:
            f.write(results)


def get_goal_stats(league):

    goal_stats = "\n\n"
    goal_stats += "    \t%s\t%s\t%s\n    \t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" \
                  % ("T.H Gls".rjust(24),
                     "T.A Gls".rjust(24),
                     "T.  Gls".rjust(24),
                     "H".rjust(6), "D".rjust(6), "A".rjust(6), "T".rjust(6),
                     "H".rjust(6), "D".rjust(6), "A".rjust(6), "T".rjust(6),
                     "H".rjust(6), "D".rjust(6), "A".rjust(6), "T".rjust(6))

    #total_home_goals = total_home_goals_in_home_wins + total_home_goals_in_draws + total_home_goals_in_away_wins
    total_home_goals_in_home_wins = 0
    total_home_goals_in_draws = 0
    total_home_goals_in_away_wins = 0
    total_home_goals = 0

    #total_draw_goals = total_home_goals_in_draws + total_away_goals_in_draws

    #total_away_goals = total_away_goals_in_home_wins + total_away_goals_in_draws + total_away_goals_in_away_wins
    total_away_goals_in_home_wins = 0
    total_away_goals_in_draws = 0
    total_away_goals_in_away_wins = 0
    total_away_goals = 0

    #total_goals = total_home_goals + total_draw_goals + total_away_goals
    total_goals_in_home_wins = 0
    total_goals_in_draws = 0
    total_goals_in_away_wins = 0
    total_goals = 0

    for year in range(1993, 2014):
        goal_stats += "\n%d\t" % year

        home_goals_query = """select sum(fthg) from football_results
        where `FTR` LIKE "%%H%%" and `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'
        union all
        select sum(fthg) from football_results
        where `FTR` LIKE "%%D%%" and `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'
        union all
        select sum(fthg)  from football_results
        where `FTR` LIKE "%%A%%" and `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'"""\
                % (league, year, year+1, league, year, year+1, league, year, year+1)
        cur.execute(home_goals_query)
        rows = cur.fetchall()
        home_goals_in_home_wins = 0
        home_goals_in_draws = 0
        home_goals_in_away_wins = 0
        counter = 0
        for row in rows:
            if counter == 0:
                home_goals_in_home_wins = row[0]
            elif counter == 1:
                home_goals_in_draws = row[0]
            else:
                home_goals_in_away_wins = row[0]
            counter += 1

        away_goals_query = """select sum(ftag) from football_results
        where `FTR` LIKE "%%H%%" and `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'
        union all
        select sum(ftag) from football_results
        where `FTR` LIKE "%%D%%" and `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'
        union all
        select sum(ftag)  from football_results
        where `FTR` LIKE "%%A%%" and `Div` = "%s" and football_results.`Date` BETWEEN '%d/07/20' AND '%d/06/30'"""\
                % (league, year, year+1, league, year, year+1, league, year, year+1)
        cur.execute(away_goals_query)
        rows = cur.fetchall()
        away_goals_in_home_wins = 0
        away_goals_in_draws = 0
        away_goals_in_away_wins = 0
        counter = 0
        for row in rows:
            if counter == 0:
                away_goals_in_home_wins = row[0]
            elif counter == 1:
                away_goals_in_draws = row[0]
            else:
                away_goals_in_away_wins = row[0]
            counter += 1

        if home_goals_in_home_wins > 0 and home_goals_in_draws > 0 and home_goals_in_away_wins > 0:
            year_home_goals = home_goals_in_home_wins + home_goals_in_draws + home_goals_in_away_wins
            goal_stats += "%s\t%s\t%s\t%s\t" % \
                          (str(home_goals_in_home_wins).rjust(6),
                           str(home_goals_in_draws).rjust(6),
                           str(home_goals_in_away_wins).rjust(6),
                           str(year_home_goals).rjust(6))

            total_home_goals_in_home_wins += home_goals_in_home_wins
            total_home_goals_in_draws += home_goals_in_draws
            total_home_goals_in_away_wins += home_goals_in_away_wins
            total_home_goals += year_home_goals

            year_away_goals = away_goals_in_home_wins + away_goals_in_draws + away_goals_in_away_wins
            goal_stats += "%s\t%s\t%s\t%s\t" % \
                          (str(away_goals_in_home_wins).rjust(6),
                           str(away_goals_in_draws).rjust(6),
                           str(away_goals_in_away_wins).rjust(6),
                           str(year_away_goals).rjust(6))

            total_away_goals_in_home_wins += away_goals_in_home_wins
            total_away_goals_in_draws += away_goals_in_draws
            total_away_goals_in_away_wins += away_goals_in_away_wins
            total_away_goals += year_away_goals

            year_goals_in_home_wins = home_goals_in_home_wins + away_goals_in_home_wins
            year_goals_in_draws = home_goals_in_draws + away_goals_in_draws
            year_goals_in_away_wins = home_goals_in_away_wins + away_goals_in_away_wins
            year_total_goals = year_goals_in_home_wins + year_goals_in_draws + year_goals_in_away_wins
            goal_stats += "%s\t%s\t%s\t%s" % \
                          (str(year_goals_in_home_wins).rjust(6),
                           str(year_goals_in_draws).rjust(6),
                           str(year_goals_in_away_wins).rjust(6),
                           str(year_total_goals).rjust(6))

            total_goals_in_home_wins += year_goals_in_home_wins
            total_goals_in_draws += year_goals_in_draws
            total_goals_in_away_wins += year_goals_in_away_wins
            total_goals += year_total_goals
    goal_stats += "\n%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" \
                  % ("tot.",
                     str(total_home_goals_in_home_wins).ljust(6),
                     str(total_home_goals_in_draws).ljust(6),
                     str(total_home_goals_in_away_wins).ljust(6),
                     str(total_home_goals).ljust(6),
                     str(total_away_goals_in_home_wins).ljust(6),
                     str(total_away_goals_in_draws).ljust(6),
                     str(total_away_goals_in_away_wins).ljust(6),
                     str(total_away_goals).ljust(6),
                     str(total_goals_in_home_wins).ljust(6),
                     str(total_goals_in_draws).ljust(6),
                     str(total_goals_in_away_wins).ljust(6),
                     str(total_goals).ljust(6))
    return goal_stats


def main():
    get_result_stats()


if __name__ == "__main__": main()