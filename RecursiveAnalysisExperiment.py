import SimpleRanking
import PageRanking
import AQBRanking
import ElephantRanking
import EloRanking
import FIFARanking
import FIFAWomenRanking
import MySQLDriver


def main():
    """
        Re-Rank teams based on the same matches but taking the previous ranking's rating
        as the initial rating for each team, until no changes occur.

        PRODUCE_FILES: default True
            If you do not wish to generate a file for each ranking set PRODUCE_FILES to False
    """
    #league = "N1"
    leagues = ["E0", "E1", "D1", "D2", "SP1", "SP2", "I1", "I2",
               "P1", "G1", "T1", "F1", "F2", "B1", "SC0", "SC1", "N1"]
    #year = 1994
    #match_set_num = 19

    exp_id = MySQLDriver.get_next_experiment_id()
    MySQLDriver.insert_experiment()

    for league in leagues:
        for year in range(1993, 2014):
            teams = MySQLDriver.get_all_league_teams_for_year(league, year)
            if teams:
                season_matches = set()
                for team in teams:
                    team_season_matches = MySQLDriver.get_random_matches_for_team(team, league, year, 52)
                    for match in team_season_matches:
                        season_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
                    del team_season_matches[:]

                list_all_matches = list(season_matches)
                list_all_matches.sort()
                # Generate final league table.
                league_table = SimpleRanking.generate_league_standings(league, year, teams, list_all_matches,
                                                                3, 1, 0, 0, 80, 999, exp_id)

                for match_set_num in range(4, 21):
                    subset_matches = set()
                    for team in teams:
                        team_sub_matches = MySQLDriver.get_random_matches_for_team(team, league, year, match_set_num)
                        for match in team_sub_matches:
                            subset_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
                        del team_sub_matches[:]

                    list_of_matches = list(subset_matches)
                    list_of_matches.sort()

                    #simple_ranking = SimpleRanking.generate_league_standings(league, year, teams, list_of_matches,
                    # 3, 2, 1, 0, match_set_num)

                    # PageRank-ing
                    next_id = MySQLDriver.get_next_ranking_id()
                    PageRanking.ext_page_ranking(league, year, teams, list_of_matches, 20, 0.00001, match_set_num,
                                                 next_id, league_table, exp_id)
                    #next_id = MySQLDriver.get_next_ranking_id()
                    #PageRanking.recursive_page_ranking(league, year, teams, list_of_matches,
                    #                                   20, 0.00001, match_set_num, next_id, league_table, exp_id)

                    # Elephant Rankings
                    #next_id = MySQLDriver.get_next_ranking_id()
                    #ElephantRanking.ext_elephant_ranking(league, year, teams, list_of_matches,
                    #                                     [5, 3, 1], [2, 0, -2], [-1, -3, -5], 0, 4, True,
                    #                                     match_set_num, next_id, league_table, exp_id)
                    #next_id = MySQLDriver.get_next_ranking_id()
                    #ElephantRanking.recursive_elephant_ranking(league, year, teams, list_of_matches,
                    #                                           [5, 3, 1], [2, 0, -2], [-1, -3, -5], 0, 4, True,
                    #                                           match_set_num, next_id, league_table, exp_id)

                    # AQB Ratings
                    #next_id = MySQLDriver.get_next_ranking_id()
                    #AQBRanking.ext_aqb_ranking(league, year, teams, list_of_matches, 1, -1, 0.25, 20, 20, 0.05, 4,
                    #                           match_set_num, next_id, league_table, exp_id)
                    #next_id = MySQLDriver.get_next_ranking_id()
                    #AQBRanking.recursive_aqb_ranking(league, year, teams, list_of_matches, 1, -1, 0.25, 20, 20, 0.05, 4,
                    #                                 match_set_num, next_id, league_table, exp_id)

                    # Elo Ratings
                    next_id = MySQLDriver.get_next_ranking_id()
                    EloRanking.ext_elo_ranking(league, year, teams, list_of_matches, 1000, 1, 0.5, 0, 24, 0,
                                               match_set_num, next_id, league_table, exp_id)
                    next_id = MySQLDriver.get_next_ranking_id()
                    EloRanking.recursive_elo_ranking(league, year, teams, list_of_matches, 1000, 1, 0.5, 0, 24, 0,
                                                     match_set_num, next_id, league_table, exp_id)

                    # FIFA Rankings
                    next_id = MySQLDriver.get_next_ranking_id()
                    FIFARanking.ext_fifa_ranking(league, year, teams, list_of_matches, 3, 1, 0, 1, 0,
                                                 match_set_num, next_id, league_table, exp_id)
                    next_id = MySQLDriver.get_next_ranking_id()
                    FIFARanking.recursive_fifa_ranking(league, year, teams, list_of_matches, 3, 1, 0, 1, 0,
                                                       match_set_num, next_id, league_table, exp_id)

                    # FIFA Women Rankings
                    next_id = MySQLDriver.get_next_ranking_id()
                    FIFAWomenRanking.ext_fifa_women_ranking(league, year, teams, list_of_matches, 1000, 24, 100,
                                                            match_set_num, next_id, league_table, exp_id)
                    next_id = MySQLDriver.get_next_ranking_id()
                    FIFAWomenRanking.recursive_fifa_women_ranking(league, year, teams, list_of_matches, 1000, 24, 100,
                                                                  match_set_num, next_id, league_table, exp_id)

                    # Elephant Scaled Rankings
                    next_id = MySQLDriver.get_next_ranking_id()
                    ElephantRanking.ext_elephant_ranking(league, year, teams, list_of_matches,
                                                         [5, 3, 1], [2, 0, -2], [-1, -3, -5], 0, 4, False,
                                                         match_set_num, next_id, league_table, exp_id)
                    next_id = MySQLDriver.get_next_ranking_id()
                    ElephantRanking.recursive_elephant_ranking(league, year, teams, list_of_matches,
                                                               [5, 3, 1], [2, 0, -2], [-1, -3, -5], 0, 4, False,
                                                               match_set_num, next_id, league_table, exp_id)
    MySQLDriver.update_experiment(exp_id)

if __name__ == "__main__": main()