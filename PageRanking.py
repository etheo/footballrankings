import MySQLDriver
from Helper import write_results_wid
from Helper import write_results
from Helper import rankings_uneven
from random import randint
from grading import RankingGrading


def ext_page_ranking(league, year, teams, list_of_matches, repeat_pr, damping_factor, num_of_matches,
                     rank_id, league_table, exp_id):
    """PageRank algorithm.

        ---> Explain algorithm.

        Args:
            league: The league under investigation. (see: leagues.txt)
            year: The year (season) used for the rankings.
            teams: Dictionary of the participating teams in the league for given year.
            list_of_matches: Supplied list of matches data used for the rankings.
            repeat_pr: # of PR repetitions.
            damping_factor: The damping factor of the algorithm.
            num_of_matches: Total number of matches in our matches dataset.
            rank_id: The ranking id (used to identify it in DB for grading).
            league_table: The final league table (should be auto-generated).
            exp_id: The experiment id.

        Returns:
            A list demonstrating the FIFA Rankings.
    """
    pr_teams = []
    winners = []
    losers = []
    counter = 0
    for team in teams:
        pr_teams.append([team, 1])
        winners.append([])
        losers.append([])
        counter += 1
    #print "%d, %d, %d, %d" % (len(teams), len(pr_teams), len(winners), len(losers))
    # Calculate summand
    summand = (1 - damping_factor) / pr_teams.__sizeof__()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(pr_teams)):
            if pr_teams[i][0] == match[2]:
                home_team = i
            elif pr_teams[i][0] == match[3]:
                away_team = i

        if match[6][0][:1] == "H":
            winners[home_team].append(away_team)
            losers[away_team].append(home_team)
        elif match[6][0][:1] == "A":
            losers[home_team].append(away_team)
            winners[away_team].append(home_team)
        else:
            #print "%d, %s, %d, %d" % (len(winners), league, year, home_team)
            winners[home_team].append(away_team)
            losers[away_team].append(home_team)
            losers[home_team].append(away_team)
            winners[away_team].append(home_team)

    for i in range(1, repeat_pr):
        for team_index in range(len(pr_teams)):
            # How often did this player either lose or get a draw?
            outlinks = losers[team_index].__sizeof__() + 1
            # How often did this player win or get draw?
            inlink_score = 0
            for opponent in winners[team_index]:
                inlink_score += pr_teams[opponent][1]
            pr_teams[team_index][1] = summand + damping_factor * (inlink_score / outlinks)

    pr_teams.sort(key=lambda x: x[1])
    var = "ID: %d Damping Factor: %s, Repeats: %s, NumOfMatches: %d (ext)" \
          % (rank_id, damping_factor, repeat_pr, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, pr_teams[::-1])

    write_results_wid(pr_teams[::-1], "pagerank", league, year, var, num_of_matches, grading, rank_id, exp_id)
    del winners[:]
    del losers[:]


def recursive_page_ranking(league, year, teams, list_of_matches, repeat_pr, damping_factor, num_of_matches,
                     rank_id, league_table, exp_id):
    """PageRank algorithm  - Recursive Ranking until no further change

        Args:
            Same as ext_ version

        Returns:
            A list demonstrating the FIFA Rankings.
    """
    pr_teams = []
    winners = []
    losers = []
    counter = 0
    for team in teams:
        pr_teams.append([team, 1])
        winners.append([])
        losers.append([])
        counter += 1
    #print "%d, %d, %d, %d" % (len(teams), len(pr_teams), len(winners), len(losers))
    # Calculate summand
    summand = (1 - damping_factor) / pr_teams.__sizeof__()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(pr_teams)):
            if pr_teams[i][0] == match[2]:
                home_team = i
            elif pr_teams[i][0] == match[3]:
                away_team = i

        if match[6][0][:1] == "H":
            winners[home_team].append(away_team)
            losers[away_team].append(home_team)
        elif match[6][0][:1] == "A":
            losers[home_team].append(away_team)
            winners[away_team].append(home_team)
        else:
            #print "%d, %s, %d, %d" % (len(winners), league, year, home_team)
            winners[home_team].append(away_team)
            losers[away_team].append(home_team)
            losers[home_team].append(away_team)
            winners[away_team].append(home_team)

    for i in range(1, repeat_pr):
        for team_index in range(len(pr_teams)):
            # How often did this player either lose or get a draw?
            outlinks = losers[team_index].__sizeof__() + 1
            # How often did this player win or get draw?
            inlink_score = 0
            for opponent in winners[team_index]:
                inlink_score += pr_teams[opponent][1]
            pr_teams[team_index][1] = summand + damping_factor * (inlink_score / outlinks)

    pr_teams.sort(key=lambda x: x[1])

    prev_ranking = []
    next_ranking = pr_teams[::-1]

    # while previous ranking is not the same with the re-ranking
    # we re-rank again until no changes occur in the ranking
    while rankings_uneven(prev_ranking, next_ranking):
        prev_ranking = next_ranking
        for team in teams:
            pr_teams.append([team, 1])
            winners.append([])
            losers.append([])
            counter += 1
        for match in list_of_matches:
            home_team = -1
            away_team = -1
            for i in range(len(next_ranking)):
                if next_ranking[i][0] == match[2]:
                    home_team = i
                elif next_ranking[i][0] == match[3]:
                    away_team = i

            if match[6][0][:1] == "H":
                winners[home_team].append(away_team)
                losers[away_team].append(home_team)
            elif match[6][0][:1] == "A":
                losers[home_team].append(away_team)
                winners[away_team].append(home_team)
            else:
                winners[home_team].append(away_team)
                losers[away_team].append(home_team)
                losers[home_team].append(away_team)
                winners[away_team].append(home_team)

        for i in range(1, repeat_pr):
            for team_index in range(len(next_ranking)):
                # How often did this player either lose or get a draw?
                outlinks = losers[team_index].__sizeof__() + 1
                # How often did this player win or get draw?
                inlink_score = 0
                for opponent in winners[team_index]:
                    inlink_score += next_ranking[opponent][1]
                next_ranking[team_index][1] = summand + damping_factor * (inlink_score / outlinks)

    next_ranking.sort(key=lambda x: x[1])

    var = "ID: %d Damping Factor: %s, Repeats: %s, NumOfMatches: %d (rec)" \
          % (rank_id, damping_factor, repeat_pr, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, next_ranking[::-1])

    write_results_wid(next_ranking[::-1], "pagerank", league, year, var, num_of_matches, grading, rank_id, exp_id)
    del winners[:]
    del losers[:]


def page_ranking(league, year, repeat_pr, damping_factor, random, rang):
    """Same as previous functions but only internal use (mainly for testing).
        random: Select random number of matches per team with upper bound rang. (default: False)
        rang: Total number of matches to select for each team or upper bound if random selection.
    """
    pr_teams = []
    winners = []
    losers = []

    match_set_num = rang
    if random:
        match_set_num = randint(1, rang)

    teams = MySQLDriver.get_all_league_teams_for_year(league, year)
    counter = 0
    for team in teams:
        pr_teams.append([team, 1])
        winners.append([])
        losers.append([])
        counter += 1

    # Calculate summand
    summand = (1 - damping_factor) / pr_teams.__sizeof__()

    all_matches = set()

    for team in teams:
        team_matches = MySQLDriver.get_random_matches_for_team(team, league, year, match_set_num)
        for match in team_matches:
            all_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
        del team_matches[:]
    
    list_of_matches = list(all_matches)
    list_of_matches.sort()

    for match in list_of_matches:
        home_team = -1
        away_team = -1
        for i in range(len(pr_teams)):
            if pr_teams[i][0] == match[2]:
                home_team = i
            elif pr_teams[i][0] == match[3]:
                away_team = i
        if match[6][0][:1] == "H":
            winners[home_team].append(away_team)
            losers[away_team].append(home_team)
        elif match[6][0][:1] == "A":
            losers[home_team].append(away_team)
            winners[away_team].append(home_team)
        else:
            winners[home_team].append(away_team)
            losers[away_team].append(home_team)
            losers[home_team].append(away_team)
            winners[away_team].append(home_team)

    for i in range(1, repeat_pr):
        for team_index in range(len(pr_teams)):
            # How often did this player either lose or get a draw?
            outlinks = losers[team_index].__sizeof__() + 1
            # How often did this player win or get draw?
            inlink_score = 0
            for opponent in winners[team_index]:
                inlink_score += pr_teams[opponent][1]
            pr_teams[team_index][1] = summand + damping_factor * (inlink_score / outlinks)

    pr_teams.sort(key=lambda x: x[1])
    var = "Damping Factor: %s, Repeats: %s\nVars: Random: %s - NumOfMatches: %d" \
          % (damping_factor, repeat_pr, random, rang)
    write_results(pr_teams[::-1], "pagerank", league, year, var)


def main():
    page_ranking("E0", 2013, 20, 0.85, False, 19)


if __name__ == "__main__": main()