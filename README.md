# README #

Each Ranking algorithm can either be executed through its main function or called externally from another class.

Read the sources for more details.

# SETUP AND RUN #

You will need Python 2.7, MySQL DB and MySQL-Python (MySQLDB)

- You can adjust the code accordingly to use another DBMS

Construct the DB (You can find them in downloads)

Update MySQLDriver with your DBinformation

Check main methods and Executor.py to understand how to execute the code

# What is this repository for? #

Football Ranking Algorithms using Python to serve as helping tool for MSc project experiments

# V1.3 #

Ranking Algorithms Implemented

Grading Algorithms Implemented

All results now linked with DB

-Predictor grading not done

Repo Owner: Manos Theodorou (University of Edinburgh)