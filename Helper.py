from datetime import datetime
import MySQLDriver
import os
import errno

PRODUCE_FILES = False


def write_results(ranking, alg, div, year, var):
    """Write Ranking results into a text file.

        Construct a file with filename: algorithmName_league_year_timestamp.txt
        containing the given ranking execution and additional information about it.

        Args:
            ranking: The actual (ordered list) ranking.
            alg: Name of the algorithm used.
            div: League/Division under investigation.
            year: Season under investigation.
            var: String variable containing additional information about this ranking execution.
    """
    rank_counter = 1
    fname = "%s_%s_%d_%s.txt" % (alg, div, year, datetime.now().strftime('%d_%m_%Y_%H%M%S'))
    filename = "Z:/Football Results/%s" % fname

    with open(filename, "w+") as f:
        f.write("Algorithm used: %s, for League: %s\nSeason(%s); vars used: %s\n\n" % (alg, div, year, var))
        f.write("%4s\t%15s\t%6s\n" % ("Rank", "Team", "Points"))

    for team in ranking:
        with open(filename, "a") as f:
            f.write('%4d\t%15s\t%6s\n' % (rank_counter, team[0], team[1]))
        rank_counter += 1


def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def write_results_wid(ranking, alg, div, year, var, num_of_matches, grading, rank_id, exp_id):
    """Write Ranking results into a text file.

        Construct a file with filename: algorithmName_UID_league_year_timestamp.txt
        containing the given ranking execution and additional information about it.

        Args:
            ranking: The actual (ordered list) ranking.
            alg: Name of the algorithm used.
            div: League/Division under investigation.
            year: Season under investigation.
            var: String variable containing additional information about this ranking execution.
            rank_id: The unique ID of this ranking execution.
    """
    if PRODUCE_FILES or alg == "simple_ranking":
        rank_counter = 1
        fname = "%s_%d_%s_%d_%s.txt" % (alg, rank_id, div, year, datetime.now().strftime('%d_%m_%Y_%H%M%S'))
        make_sure_path_exists("Z:/Football Results/experiments/experiment_%d" % exp_id)
        filename = "Z:/Football Results/experiments/experiment_%d/%s" % (exp_id, fname)

        with open(filename, "w+") as f:
            f.write("Algorithm used: %s, for League: %s\nSeason(%s); vars used: %s\n\n" % (alg, div, year, var))
            f.write("%4s\t%15s\t%6s\n" % ("Rank", "Team", "Points"))

        for team in ranking:
            with open(filename, "a") as f:
                f.write('%4d\t%15s\t%6s\n' % (rank_counter, team[0], team[1]))
            rank_counter += 1

    if alg != "league_standings":
        manhattan_dist = grading[0]
        hamming_dist = grading[1]
        lee_dist = grading[2]
        spearman_rho = grading[3]
        kendall_tau = grading[4]
        gamma = 0  # grading[5]
        MySQLDriver.insert_ranking(ranking, alg, div, year, var, num_of_matches, manhattan_dist, hamming_dist, lee_dist,
                                   spearman_rho, kendall_tau, gamma, rank_id, exp_id)


def rankings_uneven(first_ranking, second_ranking):
    """Compare rankings based on rank positions not points

        Args:
            first_ranking: first ranking to compare
            second_ranking: second ranking to compare

        Returns:
            true if rankings are not the same, false otherwise
    """
    if first_ranking.__len__() == 0 or second_ranking.__len__() == 0:
        return True

    first_ranking.sort(key=lambda x: x[1])
    second_ranking.sort(key=lambda x: x[1])

    for i in range(len(first_ranking)):
        if first_ranking[i][0] != second_ranking[i][0]:
            return True
    return False