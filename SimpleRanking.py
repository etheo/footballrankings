import MySQLDriver
import operator
from Helper import write_results
from Helper import write_results_wid
from random import randint
from grading import RankingGrading


def generate_league_standings(league, year, teams, list_of_matches, win_pts, draw_pts, loss_pts, home_adv,
                              num_of_matches, rank_id, exp_id):
    """Classic league ranking system (teams start with 0 pts)

        Used to generate final league standings

        Args:
            league: The league under investigation. (see: leagues.txt)
            year: The year (season) used for the rankings.
            teams: Dictionary of the participating teams in the league for given year.
            list_of_matches: Supplied list of matches data used for the rankings
            win_pts: The win points variable. (default: 3)
            draw_pts: The draw points variable. (default: 1)
            loss_pts: The loss points variable. (default: 0)
            home_adv: Home advantage adjustment. (custom default: 0)
            num_of_matches: Total number of matches in our matches dataset.
            rank_id: The ranking id (used to identify it in DB for grading).

        Returns:
            A list demonstrating the Simple Rankings.
    """

    # adjust win points for leagues-seasons that did not use 3-points for a win system
    if (league == "D1" or league == "D2") and year < 1995:
        win_pts = 2
    elif league == "N1" and year < 1995:
        win_pts = 2
    elif league == "P1" and year < 1996:
        win_pts = 2
    elif league == "SP1" and year < 1995:
        win_pts = 2
    elif league == "I1" and year == 1993:
        win_pts = 2
    elif league == "F1" and year == 1993:
        win_pts = 2

    for team in teams:
        teams[team] = [0, 0, 0, 0]

    for match in list_of_matches:
        res = (match[6][0][:1])
        if res == "H":
            teams[match[2]][0] += win_pts
            teams[match[2]][1] += int(match[4]) - int(match[5])
            teams[match[3]][1] += int(match[5]) - int(match[4])

            teams[match[2]][2] += int(match[4])
            teams[match[2]][3] -= int(match[5])
            teams[match[3]][2] += int(match[5])
            teams[match[3]][3] -= int(match[4])

            teams[match[3]][1] += loss_pts  # is 0
        elif res == "A":
            teams[match[3]][0] += win_pts
            teams[match[3]][1] += int(match[5]) - int(match[4])
            teams[match[2]][1] += int(match[4]) - int(match[5])

            teams[match[2]][2] += int(match[4])
            teams[match[2]][3] -= int(match[5])
            teams[match[3]][2] += int(match[5])
            teams[match[3]][3] -= int(match[4])

            teams[match[2]][1] += loss_pts  # is 0
        elif res == "D":
            teams[match[2]][0] += draw_pts
            teams[match[3]][0] += draw_pts

            teams[match[2]][2] += int(match[4])
            teams[match[2]][3] -= int(match[5])
            teams[match[3]][2] += int(match[5])
            teams[match[3]][3] -= int(match[4])

    sorted_teams = sorted(teams.iteritems(), key=operator.itemgetter(1))
    var = "ID: %d Win Pts: %d, Draw Pts: %d, Loss Pts: %d\nVars: NumOfMatches: %d - HomeAdv: %d (ext)" \
          % (rank_id, win_pts, draw_pts, loss_pts, num_of_matches, home_adv)
    write_results_wid(sorted_teams[::-1], "league_standings", league, year, var, num_of_matches, [], rank_id, exp_id)
    return sorted_teams[::-1]


def ext_simple_ranking(league, year, teams, list_of_matches, win_pts, draw_pts, loss_pts, home_adv, num_of_matches,
                       rank_id, league_table, exp_id):
    """Classic league ranking system (teams start with 0 pts)

        Args:
            league: The league under investigation. (see: leagues.txt)
            year: The year (season) used for the rankings.
            teams: Dictionary of the participating teams in the league for given year.
            list_of_matches: Supplied list of matches data used for the rankings
            win_pts: The win points variable. (default: 3)
            draw_pts: The draw points variable. (default: 1)
            loss_pts: The loss points variable. (default: 0)
            home_adv: Home advantage adjustment. (custom default: 0)
            num_of_matches: Total number of matches in our matches dataset.
            rank_id: The ranking id (used to identify it in DB for grading).

        Returns:
            A list demonstrating the Simple Rankings.
    """
    for team in teams:
        teams[team] = [0, 0, 0, 0]

    for match in list_of_matches:
        res = (match[6][0][:1])
        if res == "H":
            teams[match[2]][0] += win_pts
            teams[match[2]][1] += int(match[4]) - int(match[5])
            teams[match[3]][1] += int(match[5]) - int(match[4])

            teams[match[2]][2] += int(match[4])
            teams[match[2]][3] -= int(match[5])
            teams[match[3]][2] += int(match[5])
            teams[match[3]][3] -= int(match[4])

            teams[match[3]][1] += loss_pts  # is 0
        elif res == "A":
            teams[match[3]][0] += win_pts
            teams[match[3]][1] += int(match[5]) - int(match[4])
            teams[match[2]][1] += int(match[4]) - int(match[5])

            teams[match[2]][2] += int(match[4])
            teams[match[2]][3] -= int(match[5])
            teams[match[3]][2] += int(match[5])
            teams[match[3]][3] -= int(match[4])

            teams[match[2]][1] += loss_pts  # is 0
        elif res == "D":
            teams[match[2]][0] += draw_pts
            teams[match[3]][0] += draw_pts

            teams[match[2]][2] += int(match[4])
            teams[match[2]][3] -= int(match[5])
            teams[match[3]][2] += int(match[5])
            teams[match[3]][3] -= int(match[4])

    sorted_teams = sorted(teams.iteritems(), key=operator.itemgetter(1))
    var = "ID: %d Win Pts: %d, Draw Pts: %d, Loss Pts: %d\nVars: NumOfMatches: %d - HomeAdv: %d (ext)" \
          % (rank_id, win_pts, draw_pts, loss_pts, num_of_matches, home_adv)

    grading = RankingGrading.grade_ranking(league_table, teams[::-1])

    write_results_wid(sorted_teams[::-1], "simple_ranking", league, year, var, num_of_matches, grading, rank_id, exp_id)


def simple_ranking(league, year, win_pts, draw_pts, loss_pts, home_adv, random, rang):
    """Same as previous functions but only internal use (mainly for testing).
        random: Select random number of matches per team with upper bound rang. (default: False)
        rang: Total number of matches to select for each team or upper bound if random selection.
    """
    match_set_num = rang
    if random:
        match_set_num = randint(1, rang)

    # adjust win points for leagues-seasons that did not use 3-points for a win system
    if (league == "D1" or league == "D2") and year < 1995:
        win_pts = 2
    elif league == "N1" and year < 1995:
        win_pts = 2
    elif league == "P1" and year < 1996:
        win_pts = 2
    elif league == "SP1" and year < 1995:
        win_pts = 2
    elif league == "I1" and year == 1993:
        win_pts = 2
    elif league == "F1" and year == 1993:
        win_pts = 2

    teams = MySQLDriver.get_all_league_teams_for_year(league, year)

    for team in teams:
        teams[team] = [0, 0, 0, 0]

    for team in teams:
        matches = MySQLDriver.get_random_matches_for_team(team, league, year, match_set_num)
        for match in matches:
            res = (match[6][0][:1])
            if match[2] == team and res == "H":
                teams[team][0] += win_pts
                teams[team][1] += int(match[4]) - int(match[5])
                teams[team][2] += int(match[4])
                teams[team][3] -= int(match[5])
            elif match[3] == team and res == "A":
                teams[team][0] += win_pts
                teams[team][1] += int(match[5]) - int(match[4])
                teams[team][2] += int(match[5])
                teams[team][3] -= int(match[4])
            elif res == "D":
                teams[team][0] += draw_pts
                teams[team][2] += int(match[4])
                teams[team][3] -= int(match[4])
            elif match[2] == team and res == "A":
                teams[team][0] += loss_pts  # is 0
                teams[team][1] += int(match[4]) - int(match[5])
                teams[team][2] += int(match[4])
                teams[team][3] -= int(match[5])
            elif match[3] == team and res == "H":
                teams[team][0] += loss_pts  # is 0
                teams[team][1] += int(match[5]) - int(match[4])
                teams[team][2] += int(match[5])
                teams[team][3] -= int(match[4])
        del matches[:]

    sorted_teams = sorted(teams.iteritems(), key=operator.itemgetter(1))
    print sorted_teams
    var = "Win Pts: %d, Draw Pts: %d, Loss Pts: %d\nVars: Random: %s - NumOfMatches: %d - HomeAdv: %d" \
          % (win_pts, draw_pts, loss_pts, random, match_set_num, home_adv)
    #write_results(sorted_teams[::-1], "simple_ranking", league, year, var)


def main():
    simple_ranking("F1", 1993, 3, 1, 0, 0, False, 38)


if __name__ == "__main__": main()