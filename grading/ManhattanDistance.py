def calc_distance(league_table, ranking):
    """Calculate Manhattan's Distance for two given rankings.

        Formulae:
            distance = SUM(|xi-yi|)
            see: http://en.wikipedia.org/wiki/Taxicab_geometry

        Args:
            league_table: First ranking order.
            ranking: Second ranking order.

        Returns:
            The Manhattan Distance between the two rankings.
    """
    distance = 0

    league_position = 1
    for team in league_table:
        # find the league team's position in the ranking
        ranking_position = -1
        for i in range(len(ranking)):
            if ranking[i][0] == team[0]:
                ranking_position = i + 1
                break
        # sum += league table position - ranking position
        distance += abs(league_position - ranking_position)
        league_position += 1  # go to next team in the league table
    return distance