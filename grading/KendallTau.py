def calc_distance(league_table, ranking):
    """Calculate Kendall's Tau (rank correlation coefficient).

        Formulae:
            t = 2 * (concordant_pairs - discordant pairs) / N(N-1)
            see: http://en.wikipedia.org/wiki/Kendall_tau_rank_correlation_coefficient

        Args:
            league_table: First ranking order.
            ranking: Second ranking order.

        Returns:
            Kendall's tau a for the two given rankings.
    """
    concordant_pairs = 0
    discordant_pairs = 0
    number_of_teams = len(league_table)
    team_ranks = []

    league_position = 1
    for team in league_table:
        # find the league team's position in the ranking
        ranking_position = -1
        for i in range(len(ranking)):
            if ranking[i][0] == team[0]:
                ranking_position = i + 1
                break
        team_ranks.append([team[0], league_position, ranking_position])
        league_position += 1  # go to next team in the league table

    for team in range(len(team_ranks)):
        for right_team in range(team+1, len(team_ranks)):
            if (team_ranks[team][1] < team_ranks[right_team][1] and team_ranks[team][2] < team_ranks[right_team][2]) \
                    or (team_ranks[team][1] < team_ranks[right_team][1]
                        and team_ranks[team][2] < team_ranks[right_team][2]):
                    concordant_pairs += 1
            else:
                discordant_pairs += 1
                # just for debugging
                if team_ranks[team][1] == team_ranks[right_team][1]\
                        and team_ranks[team][2] == team_ranks[right_team][2]:
                    print "equal for %s - %s:" % (team_ranks[team][0], team_ranks[right_team][0])

    tau = 2 * (float(concordant_pairs - discordant_pairs) / (pow(number_of_teams, 2) - number_of_teams))
    return tau