def calc_distance(league_table, ranking):
    """Calculate Spearman's Rho (rank correlation coefficient)

        Formulae:
            1 - ( 6 * SUM(|xi-yi| / N(N^2-1) )
            see: http://en.wikipedia.org/wiki/Spearman's_rank_correlation_coefficient

        Args:
            league_table: First ranking order.
            ranking: Second ranking order.

        Returns:
            Spearman's Rho for the two given rankings.
    """
    distance = 0
    number_of_teams = len(league_table)

    league_position = 1
    for team in league_table:
        # find the league team's position in the ranking
        ranking_position = -1
        for i in range(len(ranking)):
            if ranking[i][0] == team[0]:
                ranking_position = i + 1
                break
        # sum += league table position - ranking position
        distance += pow(league_position - ranking_position, 2)
        league_position += 1  # go to next team in the league table
    rho = 1 - (6 * (float(distance) / (pow(number_of_teams, 3) - number_of_teams)))
    return rho