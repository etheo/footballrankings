def calc_distance(league_table, ranking):
    """Calculate Hamming Distance for two given rankings.

        Formulae:
            For every ranking mismatch increment distance by 1.
            see: http://en.wikipedia.org/wiki/Hamming_distance

        Args:
            league_table: First ranking order.
            ranking: Second ranking order.

        Returns:
            The Hamming Distance between the two rankings.
    """
    distance = 0

    league_position = 1
    for team in league_table:
        # find the league team's position in the ranking
        ranking_position = -1
        for i in range(len(ranking)):
            if ranking[i][0] == team[0]:
                ranking_position = i + 1
                break
        if league_position != ranking_position:
            distance += 1
        league_position += 1  # go to next team in the league table
    return distance