import HammingDistance
import KendallTau
import LeeDistance
import ManhattanDistance
import SpearmanRho


def grade_ranking(league_table, ranking):
    """ Assisting function to grade a given ranking and store the results in the database

        Args:
            league_table: the final league table
            ranking: the ranking to be graded (investigation)
            algorithm: the algorithm used for the ranking
    """
    manhattan_dist = ManhattanDistance.calc_distance(league_table, ranking)
    hamming_dist = HammingDistance.calc_distance(league_table, ranking)
    lee_dist = LeeDistance.calc_distance(league_table, ranking)
    rho = SpearmanRho.calc_distance(league_table, ranking)
    tau = KendallTau.calc_distance(league_table, ranking)
    return [manhattan_dist, hamming_dist, lee_dist, rho, tau]