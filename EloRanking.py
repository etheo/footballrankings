import MySQLDriver
from Helper import write_results
from Helper import write_results_wid
from Helper import rankings_uneven
from random import randint
from grading import RankingGrading


def ext_elo_ranking(league, year, teams, list_of_matches, start_pts, win_pts, draw_pts, loss_pts, match_factor,
                    home_adv, num_of_matches, rank_id, league_table, exp_id):
    """Elo ratings system (to be used externally - see next function to be used internally).

        Originally uses all historical match results data.
        Starting elo points according to when the first match was played or fixed 1000-1200-1500.
        Formulae:
            NewRating = OldRating + goal_diff * match_factor * (match_result_points - win_expectancy)
        see: http://www.eloratings.net/

        Args:
            league: The league under investigation. (see: leagues.txt)
            year: The year (season) used for the rankings.
            teams: Dictionary of the participating teams in the league for given year.
            list_of_matches: Supplied list of matches data used for the rankings.
            start_pts: The initial points for each team. (default: 1000)
            win_pts: The win points variable. (default: 1)
            draw_pts: The draw points variable. (default: 0.5)
            loss_pts: The loss points variable. (default: 0)
            match_factor: Match importance factor.
                60 for World Cup finals;
                50 for continental championship finals and major intercontinental tournaments;
                40 for World Cup and continental qualifiers and major tournaments;
                30 for all other tournaments;
                20 for friendly matches.
            home_adv: Home advantage adjustment. (default: 100)
            num_of_matches: Total number of matches in our matches dataset.
            rank_id: The ranking id (used to identify it in DB for grading).
            league_table: The final league table (should be auto-generated).
            exp_id: The experiment id.

        Returns:
            A list demonstrating the Elo Ratings.
    """
    elo_teams = []
    for team in teams:
        elo_teams.append([team, start_pts])
    elo_teams.sort()

    for match in list_of_matches:
        goal_diff = abs(int(match[4]) - int(match[5]))
        home_team = -1
        away_team = -1
        for i in range(len(elo_teams)):
            if elo_teams[i][0] == match[2]:
                home_team = i
            elif elo_teams[i][0] == match[3]:
                away_team = i

        expected_home = 1 / (1 + 10 **
                             (-1 * (abs(elo_teams[home_team][1] - (elo_teams[away_team][1])) + home_adv) / 400))
        expected_away = 1 - expected_home
        if match[6][0][:1] == "H":
            elo_teams[home_team][1] += goal_diff * match_factor * (win_pts - expected_home)
            elo_teams[away_team][1] += goal_diff * match_factor * (loss_pts - expected_away)
        elif match[6][0][:1] == "A":
            elo_teams[home_team][1] += goal_diff * match_factor * (loss_pts - expected_home)
            elo_teams[away_team][1] += goal_diff * match_factor * (win_pts - expected_away)
        else:
            elo_teams[home_team][1] += goal_diff * match_factor * (draw_pts - expected_home)
            elo_teams[away_team][1] += goal_diff * match_factor * (draw_pts - expected_away)

    elo_teams.sort(key=lambda x: x[1])
    var = "ID: %d Win Pts: %d, Draw Pts: %d, Loss Pts: %d\nVars: Match Factor: %d-HomeAdv: %d, NumOfMatches: %d (ext)"\
          % (rank_id, win_pts, draw_pts, loss_pts, match_factor, home_adv, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, elo_teams[::-1])

    write_results_wid(elo_teams[::-1], "elo_ranking", league, year, var, num_of_matches, grading, rank_id, exp_id)


def recursive_elo_ranking(league, year, teams, list_of_matches, start_pts, win_pts, draw_pts, loss_pts, match_factor,
                          home_adv, num_of_matches, rank_id, league_table, exp_id):
    """Elo ratings system  - Recursive Ranking until no further change

        Args:
            Same as ext_ version

        Returns:
            A list demonstrating the Elo Ratings.
    """
    elo_teams = []
    for team in teams:
        elo_teams.append([team, start_pts])
    elo_teams.sort()

    for match in list_of_matches:
        goal_diff = abs(int(match[4]) - int(match[5]))
        home_team = -1
        away_team = -1
        for i in range(len(elo_teams)):
            if elo_teams[i][0] == match[2]:
                home_team = i
            elif elo_teams[i][0] == match[3]:
                away_team = i

        expected_home = 1 / (1 + 10 **
                             (-1 * (abs(elo_teams[home_team][1] - (elo_teams[away_team][1])) + home_adv) / 400))
        expected_away = 1 - expected_home
        if match[6][0][:1] == "H":
            elo_teams[home_team][1] += goal_diff * match_factor * (win_pts - expected_home)
            elo_teams[away_team][1] += goal_diff * match_factor * (loss_pts - expected_away)
        elif match[6][0][:1] == "A":
            elo_teams[home_team][1] += goal_diff * match_factor * (loss_pts - expected_home)
            elo_teams[away_team][1] += goal_diff * match_factor * (win_pts - expected_away)
        else:
            elo_teams[home_team][1] += goal_diff * match_factor * (draw_pts - expected_home)
            elo_teams[away_team][1] += goal_diff * match_factor * (draw_pts - expected_away)

    elo_teams.sort(key=lambda x: x[1])

    prev_ranking = []
    next_ranking = elo_teams[::-1]

    # while previous ranking is not the same with the re-ranking
    # we re-rank again until no changes occur in the ranking
    while rankings_uneven(prev_ranking, next_ranking):
        prev_ranking = next_ranking
        for match in list_of_matches:
            goal_diff = abs(int(match[4]) - int(match[5]))
            home_team = -1
            away_team = -1
            for i in range(len(next_ranking)):
                if next_ranking[i][0] == match[2]:
                    home_team = i
                elif next_ranking[i][0] == match[3]:
                    away_team = i

            expected_home = 1 / (1 + 10 **
                                 (-1*(abs(next_ranking[home_team][1] - (next_ranking[away_team][1])) + home_adv) / 400))
            expected_away = 1 - expected_home
            if match[6][0][:1] == "H":
                next_ranking[home_team][1] += goal_diff * match_factor * (win_pts - expected_home)
                next_ranking[away_team][1] += goal_diff * match_factor * (loss_pts - expected_away)
            elif match[6][0][:1] == "A":
                next_ranking[home_team][1] += goal_diff * match_factor * (loss_pts - expected_home)
                next_ranking[away_team][1] += goal_diff * match_factor * (win_pts - expected_away)
            else:
                next_ranking[home_team][1] += goal_diff * match_factor * (draw_pts - expected_home)
                next_ranking[away_team][1] += goal_diff * match_factor * (draw_pts - expected_away)

    next_ranking.sort(key=lambda x: x[1])

    var = "ID: %d Win Pts: %d, Draw Pts: %d, Loss Pts: %d\nVars: Match Factor: %d-HomeAdv: %d, NumOfMatches: %d (rec)"\
          % (rank_id, win_pts, draw_pts, loss_pts, match_factor, home_adv, num_of_matches)

    grading = RankingGrading.grade_ranking(league_table, next_ranking[::-1])

    write_results_wid(next_ranking[::-1], "elo_ranking", league, year, var, num_of_matches, grading, rank_id, exp_id)


def elo_ranking(league, year, start_pts, win_pts, draw_pts, loss_pts, match_factor, home_adv, random, rang):
    """Same as previous functions but only internal use (mainly for testing).
        random: Select random number of matches per team with upper bound rang. (default: False)
        rang: Total number of matches to select for each team or upper bound if random selection.
    """
    match_set_num = rang
    if random:
        match_set_num = randint(1, rang)

    teams = MySQLDriver.get_all_league_teams_for_year(league, year)
    elo_teams = []
    for team in teams:
        elo_teams.append([team, start_pts])
    elo_teams.sort()
    
    all_matches = set()

    for team in teams:
        team_matches = MySQLDriver.get_random_matches_for_team(team, league, year, match_set_num)
        for match in team_matches:
            all_matches.add((match[0], match[1], match[2], match[3], match[4], match[5], match[6]))
        del team_matches[:]
    
    list_of_matches = list(all_matches)
    list_of_matches.sort()

    for match in list_of_matches:
        goal_diff = abs(int(match[4]) - int(match[5]))
        home_team = -1
        away_team = -1
        for i in range(len(elo_teams)):
            if elo_teams[i][0] == match[2]:
                home_team = i
            elif elo_teams[i][0] == match[3]:
                away_team = i

        expected_home = 1 / (1 + 10 **
                             (-1 * (abs(elo_teams[home_team][1] - (elo_teams[away_team][1])) + home_adv) / 400))
        expected_away = 1 - expected_home
        if match[6][0][:1] == "H":
            elo_teams[home_team][1] += goal_diff * match_factor * (win_pts - expected_home)
            elo_teams[away_team][1] += goal_diff * match_factor * (loss_pts - expected_away)
        elif match[6][0][:1] == "A":
            elo_teams[home_team][1] += goal_diff * match_factor * (loss_pts - expected_home)
            elo_teams[away_team][1] += goal_diff * match_factor * (win_pts - expected_away)
        else:
            elo_teams[home_team][1] += goal_diff * match_factor * (draw_pts - expected_home)
            elo_teams[away_team][1] += goal_diff * match_factor * (draw_pts - expected_away)

    elo_teams.sort(key=lambda x: x[1])
    var = "Win Pts: %d, Draw Pts: %d, Loss Pts: %d\nVars:Random: %s - NumOfMatches: %d - Match Factor: %d-HomeAdv: %d "\
          % (win_pts, draw_pts, loss_pts, random, rang, match_factor, home_adv)
    write_results(elo_teams[::-1], "elo_ranking", league, year, var)


def main():
    elo_ranking("E0", 2013, 1000, 1, 0.5, 0, 24, 100, False, 38)


if __name__ == "__main__": main()